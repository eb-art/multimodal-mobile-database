function [ ] = preprocess_imgs( input_dir, output_dir, ext )
%PREPROCESS_IMGS Preprocess images (currently only split)
%input_dir - input directory from which the images are read
%output_dir - output directory to which the output images are saved
%ext - extension

listing = dir([input_dir '\\*.' ext]);
for i = 1:numel(listing)
   image = imread([input_dir '\\' listing(i).name]);
   [im_left, im_right] = imsplit(image);
   imwrite(im_left, [output_dir '\\' listing(i).name(1:end-4) '_P.' ext]);
   imwrite(im_right, [output_dir '\\' listing(i).name(1:end-4) '_L.' ext]);
end


end

