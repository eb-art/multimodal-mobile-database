Data = Oczy(:,:);

ID_location = [34 38;34 38];
Session_location = [42 44;40 42];
Eye_location = [40 40;5 5];
N_location = [46 5;44 7];

%okreslenie, co powinno byc na 'discriminator' miejscu, zeby to byl 1
%typ, a nie 2
discriminator = 40;
discriminator_feats = ['L', 'P'];
types_names = {'one eye', 'both eyes'};

Filenames1 = table2cell(Data(:,1));
Filenames2 = table2cell(Data(:,2));
Scores = table2cell(Data(:,3));

n_comps = numel(Filenames1);

for i = 1:n_comps
   Filenames1{i} = char(Filenames1{i});
   Filenames2{i} = char(Filenames2{i});
end

Subject_ID = {};
Candidate_ID = {};
Subject_Session = {};
Candidate_Session = {};
Subject_N = {};
Candidate_N = {};
Subject_Type = {};
Candidate_Type = {};
Subject_Eye = {};
Candidate_Eye = {};

for i = 1:n_comps
   Scores{i} = double(Scores{i});
   types = [1 1];
   if ~ismember(Filenames1{i}(discriminator), discriminator_feats)
       types(1) = 2;
   end
   if ~ismember(Filenames2{i}(discriminator), discriminator_feats)
       types(2) = 2;
   end
   
   Subject_ID{i} = Filenames1{i}(ID_location(types(1),1):ID_location(types(1),2));
   Candidate_ID{i} = Filenames2{i}(ID_location(types(2),1):ID_location(types(2),2));
   Subject_Session{i} = Filenames1{i}(Session_location(types(1),1):Session_location(types(1),2));
   Candidate_Session{i} = Filenames2{i}(Session_location(types(2),1):Session_location(types(2),2));
   Subject_N{i} = Filenames1{i}(N_location(types(1),1):end-N_location(types(1),2));
   Candidate_N{i} = Filenames2{i}(N_location(types(2),1):end-N_location(types(2),2));
   Subject_Type{i} = types_names{types(1)};
   Candidate_Type{i} = types_names{types(2)};
   
   if types(1) == 1
       Subject_Eye{i} = Filenames1{i}(Eye_location(types(1),1):Eye_location(types(1),2));
   else
       Subject_Eye{i} = Filenames1{i}(end-Eye_location(types(1),1):end-Eye_location(types(1),2));
   end
   if types(2) == 1
       Candidate_Eye{i} = Filenames2{i}(Eye_location(types(2),1):Eye_location(types(2),2));
   else
       Candidate_Eye{i} = Filenames2{i}(end-Eye_location(types(2),1):end-Eye_location(types(2),2));
   end
end

Table_ALL = table(Subject_ID', Subject_Session', Subject_N', Subject_Type', Subject_Eye', Candidate_ID', Candidate_Session', Candidate_N', Candidate_Type', Candidate_Eye', Scores);
Table_ALL.Properties.VariableNames = {'SubjectID', 'SubjectSession', 'SubjectN', 'SubjectType', 'SubjectEye', 'CandidateID', 'CandidateSession', 'CandidateN', 'CandidateType', 'CandidateEye', 'Score'};