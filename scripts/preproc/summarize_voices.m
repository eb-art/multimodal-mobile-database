in_dir = 'R:\\VOICE_DB\\Wav\\';
out_dir = 'R:\\VOICE_DB\\';

IDs = {};
Types = {};
Session1 = {};
Session3 = {};
Session5 = {};
Session6 = {};

for i = 1:54
    all_files = dir([in_dir '**\\' id{i} '*.wav']);
    sessions = cell(numel(all_files),1);
    for j = 1:numel(all_files)
        sessions_split = strsplit(all_files(j).name,'_');
        sessions{j} = sessions_split{2};
    end
    
    subfolder = 'Male';
    if plec{i} == 'F'
        subfolder = 'Female';
    end
    
    IDs{i} = id{i};
    Types{i} = plec{i};
    Session1{i} = numel(find(cell2mat(strfind(sessions,'S01'))));
    Session3{i} = numel(find(cell2mat(strfind(sessions,'S03'))));
    Session5{i} = numel(find(cell2mat(strfind(sessions,'S05'))));
    Session6{i} = numel(find(cell2mat(strfind(sessions,'S06'))));
    
end

data = table(IDs', Types', Session1', Session3', Session5', Session6');
data.Properties.VariableNames = {'ID', 'Plec', 'S01', 'S03', 'S05', 'S06'};
writetable(data,'glosy_podsumowanie.csv','Delimiter',';');