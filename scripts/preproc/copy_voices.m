in_dir = 'R:\\VOICE_DB\\Wav\\';
out_dir = 'R:\\VOICE_DB\\';

for i = 1:54
    all_files = dir([in_dir '**\\' id{i} '*.wav']);
    sessions = cell(numel(all_files),1);
    for j = 1:numel(all_files)
        sessions_split = strsplit(all_files(j).name,'_');
        sessions{j} = sessions_split(2);  
    end
    
    subfolder = 'Male';
    if plec{i} == 'F'
        subfolder = 'Female';
    end
    
    for j = 1:numel(all_files)
       copyfile([all_files(j).folder '\\' all_files(j).name], [out_dir '\\' subfolder '\\' sessions{j}{1} '\\' all_files(j).name]); 
    end
    
end

