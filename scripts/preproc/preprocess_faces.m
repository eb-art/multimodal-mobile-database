Fejsy=readtable(['VeriLook_results_09_58_27.csv'], 'Delimiter',';');
Data = Fejsy(:,:);

a = char(Fejsy{1,1});

ID_index = strfind(a,'u0')+1;
session_index = strfind(a,'s0')+1;

ID_location = [ID_index ID_index+3];
Session_location = [session_index session_index+1];
Type_location = [session_index+3 7];
N_location = [5 5]; 

Filenames1 = table2cell(Data(:,1));
Filenames2 = table2cell(Data(:,2));
Scores = table2cell(Data(:,3));

n_comps = numel(Filenames1);

for i = 1:n_comps
   Filenames1{i} = char(Filenames1{i});
   Filenames2{i} = char(Filenames2{i});
end

Subject_ID = {};
Candidate_ID = {};
Subject_Session = {};
Candidate_Session = {};
Subject_N = {};
Candidate_N = {};
Subject_Type = {};
Candidate_Type = {};

for i = 1:n_comps
   Scores{i} = double(Scores{i});
   Subject_ID{i} = str2num(Filenames1{i}(ID_location(1):ID_location(2)));
   Candidate_ID{i} = str2num(Filenames2{i}(ID_location(1):ID_location(2)));
   Subject_Session{i} = str2num(Filenames1{i}(Session_location(1):Session_location(2)));
   Candidate_Session{i} = str2num(Filenames2{i}(Session_location(1):Session_location(2)));
   Subject_N{i} = str2num(Filenames1{i}(end-N_location(1):end-N_location(2)));
   Candidate_N{i} = str2num(Filenames2{i}(end-N_location(1):end-N_location(2)));
   Subject_Type{i} = Filenames1{i}(Type_location(1):end-Type_location(2));
   Candidate_Type{i} = Filenames2{i}(Type_location(1):end-Type_location(2));
end

Table_ALL = table(Subject_ID', Subject_Session', Subject_N', Subject_Type', Candidate_ID', Candidate_Session', Candidate_N', Candidate_Type', Scores);
Table_ALL.Properties.VariableNames = {'SubjectID', 'SubjectSession', 'SubjectN', 'SubjectType', 'CandidateID', 'CandidateSession', 'CandidateN', 'CandidateType', 'Score'};