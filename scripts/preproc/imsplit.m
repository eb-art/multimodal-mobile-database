function [ LEFT, RIGHT ] = imsplit( image )

LEFT = image(:,1:size(image,2)/2,:);
RIGHT = image(:,size(image,2)/2 + 1:end,:);

end

