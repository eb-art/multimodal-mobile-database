function [CorrectMatch,FalseMatch] = acROC(WITHIN,BETWEEN,WITHINlowerThanBETWEEN,maxROCpoints)


%% check for errors in input params
[sW1,sW2] = size(WITHIN);
if (sW2 < sW1) WITHIN = WITHIN.';
end
if (sW2 == sW1) error('WITHIN must be a vector');
end

[sB1,sB2] = size(BETWEEN);
if (sB2 < sB1) BETWEEN = BETWEEN.';
end
if (sB2 == sB1) error('BETWEEN must be a vector');
end


%% optional params
if ~exist('maxROCpoints','var') || isempty(maxROCpoints),
    maxROCpoints = inf;
end


%% to the job ...

% get the threshold range
ALL = [WITHIN BETWEEN];
minThr = min(ALL);
maxThr = max(ALL);

% set the ROC step
ROCpoints = min([max([sW1 sW2 sB1 sB2]) maxROCpoints]);
ROCstep = (maxThr - minThr) / ROCpoints;

% calculate error estimators as functions of the threshold
i = 1;
CorrectMatch = single(zeros(1,ROCpoints));
FalseMatch = single(zeros(1,ROCpoints));

if (WITHINlowerThanBETWEEN)
    for t=minThr:ROCstep:maxThr
        CorrectMatch(i) = length(find(WITHIN<=t))/length(WITHIN);
        FalseMatch(i) = length(find(BETWEEN<=t))/length(BETWEEN);
        i = i + 1;
    end
else
    for t=minThr:ROCstep:maxThr
        CorrectMatch(i) = length(find(WITHIN>=t))/length(WITHIN);
        FalseMatch(i) = length(find(BETWEEN>=t))/length(BETWEEN);
        i = i + 1;
    end
end
