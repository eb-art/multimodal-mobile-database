close all;
clear all;
%% ------------------------------------------------------------

in='h:\MOBiBITs\MOBiBITs_BADANIA\';
out=[in 'FACE_results\'];
if ~exist(out)
    mkdir(out)
end

file_list=dir([in 'Twarze*csv']);
execution_date=datestr(now,'dd_mm_yyyy');



for file_index = 1:numel(file_list)
    
    file_name = 'FACE';%file_list(file_index).name;
    
    %splited_name = strsplit(file_name,'_');
    
    score_table=readtable([in file_list(file_index).name], 'Delimiter',';');
    %,'Format','%s%s%s%s%s%s%s%s%d');
    score_table(score_table{:,9}==2822,:)=[];
    score_table{:,10}=0;
    
    %% --------------------match adding -----------------------------------
    
    Y = find(score_table{:,1}==score_table{:,5});
    score_table{Y,10}=1;
    %----------------------------------------------------------------------
    users = unique([score_table{:,1}; score_table{:,5}]);
    face_position = unique(score_table{:,8});
    
    %     resultsFile = fopen([out 'Sorted_comparisons_' file_name '_' execution_date '.csv'],'w');
    %     csvHeader = [{'User_ID_subject'} 'Session_subject' 'SubjectType' 'User_ID2_candidate'  'Session_candidate'  'CandidateType' 'Score' 'Match' 'lp.' 'RANK'];
    %     fprintf(resultsFile,'%s, %s, %s, %s, %s, %s, %s, %s, %s, %s\n',csvHeader{1,:});
    iter = 1;
    for session_index_1 = 1              %:2:5
        for session_index_2 = 1:2:5
            
            score_table_1=[];
            
            session_name_1 = num2str(session_index_1);
            session_name_2 = num2str(session_index_2);
            
            resultsFile = fopen([out 'CMC_results_' file_name '_s0' session_name_1 '_s0' session_name_2 '_' execution_date '.csv'],'w');
            csvHeader = [{'User_ID_subject'}  'SubjectType' 'User_ID2_candidate'    'CandidateType' 'Score' 'Match' 'lp.' 'RANK'];
            fprintf(resultsFile,'%s,  %s, %s, %s, %s, %s, %s, %s\n',csvHeader{1,:});
            
            
            % session part
            y_1 =   find(score_table{:,2}==session_index_1 & ...
                score_table{:,6}==session_index_2);
            if numel(y_1)>0
                score_table_1 = score_table(y_1,:);
                
                %% ----------- curves -------------------------------------
                GEN_scores = score_table_1{find(score_table_1{:,10}==1),9};
                IMP_scores = score_table_1{find(score_table_1{:,10}==0),9};
                
                %% ---------- FAR and FRR ---------------------------------
                [RES DIST] = ac_eer_mob(GEN_scores,IMP_scores,1);
                EER(iter) = RES.EER;
                iter = iter+1;
                figure
                ac_dist_plot_mob(DIST,RES,'simple',1);
                
                % ----------- ROC curve -----------------------------------
                [CMR, FMR] = acROC_mob(GEN_scores,IMP_scores,0);
                figure(4), hold on
                plot(FMR,CMR,':','LineWidth',4);
                text(0.15,0.9,'EER line','FontSize',20)
                grid on;
                grid minor
                set(gca,'xtick',0:0.1:1)
                set(gca,'ytick',0:0.1:1)
                line([1,0],[0,1],'Color',[0.6, 0.6, 0.6], 'LineWidth',2, ...
                    'LineStyle','--')
                
                set(gca,'FontSize',18)
                %                 plot_legend = legend(['session 1 vs. session ' ...
                %                     num2str(session_index_2) ...
                %                     ' EER = ' num2str(RES.EER*100,'%1.2f') '%'], ...
                %                     'Location','southeast');
                %                 set(plot_legend,'FontSize',24);
                %set(gca, 'YScale', 'log')
                hold off
                %% ----------- ROC curve END-------------------------------
                
                f_RANK=zeros(1, numel(users));
                f_lp=zeros(1, numel(users));
                
                for user_index_1= 1:numel(users)
                    for position_index=1:numel(face_position)
                        
                        score_table_2=[];
                        
                        y_2 = find(score_table_1{:,1} == users(user_index_1) & ...
                            strcmp(score_table_1{:,4}, face_position(position_index)));
                        if numel(y_2)>0
                            score_table_2 = score_table_1(y_2,:);
                            
                            [x_score,y_score]=sort(score_table_2{:,9},'descend');
                            score_table_part_sorted_1 = score_table_2(y_score,:);
                            %------------------------------------------------------
                            %                             if (score_table_part_sorted_1{1,9}==2822)
                            %                                 copy =  score_table_part_sorted_1(2:end,:);
                            %                                 score_table_part_sorted_1 = copy;
                            %                             end
                            %---------------------------lp---------------------------
                            [A, B] = unique(score_table_part_sorted_1{:,5});
                            score_table_3 = score_table_part_sorted_1(B,:);
                            
                            [x_score2,y_score2]=sort(score_table_3{:,9},'descend');
                            score_table_part_sorted_2 = score_table_3(y_score2,:);
                            
                            
                            sorted_score = sort(unique(score_table_part_sorted_2{:,9}),'descend');
                            
                            lp =  find(score_table_part_sorted_2{:,5} == score_table_part_sorted_2{1,1});
                            if numel(lp)>0
                                RANK = find(sorted_score == score_table_part_sorted_2{lp,9});
                                
                                %                                 if RANK>1
                                %                                     RANK
                                %                                 end
                                f_lp(lp)= f_lp(lp) + 1;
                                f_RANK(RANK) = f_RANK(RANK) + 1;
                                
                            end
                            %TODO   - ZAPISYWANIE
                            %       - ROBI� KRZYWE
                        end
                    end
                end
                
            end
            CMC_lp=0;
            CMC_RANK=0;
            for CMC_index=2:numel(users)
                CMC_lp(CMC_index) = CMC_lp(CMC_index-1) + f_lp(CMC_index-1);
                CMC_RANK(CMC_index) = CMC_RANK(CMC_index-1) + f_RANK(CMC_index-1);
            end
            figure(5), hold on, grid on; grid minor
            plot(CMC_RANK./max(CMC_RANK), 'LineWidth',4, 'LineStyle','-.')
            xlabel('RANK')
            ylabel('')
            set(gca,'FontSize',18)
            axis([0 CMC_index 0 1])
            %set(gca, 'YScale', 'log')
            hold off
            %             subplot(1,2,1), plot(CMC_lp./max(CMC_lp))
            %             subplot(1,2,2), plot(CMC_RANK./max(CMC_RANK))
        end
    end
    
    
    figure(4)
    plot_legend = legend(['session 1 vs. session 1: EER = ' num2str(EER(1)*100,'%1.2f') '%'], ...
        ['session 1 vs. session 2: EER = ' num2str(EER(2)*100,'%1.2f') '%'], ...
        ['session 1 vs. session 3: EER = ' num2str(EER(3)*100,'%1.2f') '%'],...
        'Location','southeast');
    set(plot_legend,'FontSize',24);
end

