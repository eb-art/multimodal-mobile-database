function ac_dist_plot(DIST,RES,method,ImpSmallerThanGen);
% (c) Adam Czajka, a.czajka@ia.pw.edu.pl


%% check the direction of integration
if (isempty(who('ImpSmallerThanGen'))) ImpSmallerThanGen = 0; end

if (ImpSmallerThanGen ~= 0)
    FRColor = 'r';
    FAColor = 'g';
else
    FRColor = 'g';
    FAColor = 'r';
end

EERColor = 'b';

figure(1)
FigMargin = 0.05;

maxxFRR = max(DIST.dxFRR);
minxFAR = min(DIST.dxFAR);
minmin = min([minxFAR min(DIST.dxFRR)]);
maxmax = max([maxxFRR max(DIST.dxFAR)]);

hold on

if (minmin ~= maxmax)
axis([...
    minmin-FigMargin*(maxmax-minmin) ...
    maxmax+FigMargin*(maxmax-minmin) ...
    -FigMargin ...
    1+FigMargin]);
end

%% Plotting method
switch lower(method)

    case 'simple'
        % plot cumulative dists + the EER
        grid on
        set(plot(DIST.dxFRR,DIST.dyFRR,FRColor),'LineWidth',2);
        set(plot(DIST.dxFAR,DIST.dyFAR,FAColor),'LineWidth',2);
        set(plot(RES.THR,RES.EER,['.' EERColor]),'MarkerSize',20);

    otherwise
        % plot all the details
        
        %% FRR
        for i=1:length(DIST.dxFRR)-1
            set(line([DIST.dxFRR(i) DIST.dxFRR(i+1)],[DIST.dyFRR(i+1) DIST.dyFRR(i+1)]),'Color',FRColor,'LineWidth',2)
        end
        %set(line([max(DIST.dxFRR) max(DIST.dxFAR)],[0 0]),'Color',FRColor,'LineWidth',2)

        set(plot(DIST.dxFRR,[DIST.dyFRR(2:length(DIST.dyFRR)) 1],['o' FRColor]),'MarkerSize',7);
        set(plot(DIST.dxFRR,DIST.dyFRR,['.' FRColor]),'MarkerSize',18);



        %% FAR
        for i=1:length(DIST.dxFAR)-1
            set(line([DIST.dxFAR(i) DIST.dxFAR(i+1)],[DIST.dyFAR(i) DIST.dyFAR(i)]),'Color',FAColor,'LineWidth',1)
        end
        %set(line([min(DIST.dxFAR) min(DIST.dxFRR)],[0 0]),'Color',FAColor,'LineWidth',1)

        set(plot(DIST.dxFAR,DIST.dyFAR,['.' FAColor]),'MarkerSize',15);
        set(plot(DIST.dxFAR,[0 DIST.dyFAR(1:length(DIST.dyFAR)-1)],['o' FAColor]),'MarkerSize',5);


        %% worst result lines
        set(line([maxxFRR maxxFRR],[0 1]),'Color',FRColor,'LineStyle',':');
        set(line([minxFAR minxFAR],[0 1]),'Color',FAColor,'LineStyle',':');


        if (ImpSmallerThanGen == 0)
            %% ZeroFAR
            set(line([minmin maxmax],[RES.ZeroFAR RES.ZeroFAR]),'Color',FRColor,'LineStyle',':');
            set(text(max(DIST.dxFRR),0.5*FigMargin+RES.ZeroFAR,['    FRR @ zeroFAR = ' num2str(RES.ZeroFAR) '    ']),'Color',FRColor,'HorizontalAlignment','right');
            %% ZeroFRR
            set(line([minmin maxmax],[RES.ZeroFRR RES.ZeroFRR]),'Color',FAColor,'LineStyle',':');
            set(text(min(DIST.dxFAR),0.5*FigMargin+RES.ZeroFRR,['    FAR @ zeroFRR = ' num2str(RES.ZeroFRR) '    ']),'Color',FAColor);
        else
            %% ZeroFAR
            set(line([minmin maxmax],[RES.ZeroFAR RES.ZeroFAR]),'Color',FAColor,'LineStyle',':');
            set(text(max(DIST.dxFRR),0.5*FigMargin+RES.ZeroFAR,['    FRR @ zeroFAR = ' num2str(RES.ZeroFAR) '    ']),'Color',FAColor,'HorizontalAlignment','right');
            %% ZeroFRR
            set(line([minmin maxmax],[RES.ZeroFRR RES.ZeroFRR]),'Color',FRColor,'LineStyle',':');
            set(text(min(DIST.dxFAR),0.5*FigMargin+RES.ZeroFRR,['    FAR @ zeroFRR = ' num2str(RES.ZeroFRR) '    ']),'Color',FRColor);
       end            

        %% EER
        set(line([minmin RES.THR],[RES.EER RES.EER]),'Color',EERColor,'LineStyle',':');
        set(line([RES.THR RES.THR],[0 RES.EER]),'Color',EERColor,'LineStyle',':');
        set(plot(RES.THR,RES.EER,['.' EERColor]),'MarkerSize',15);
        set(text(minmin,0.5*FigMargin+RES.EER,['    EER = ' num2str(RES.EER) '    ']),'Color',EERColor);
 
end

title('Estymacja bledow FRR i FAR w funkcji progu decyzyjnego')
xlabel('Odleglosc Hamminga')
ylabel('Poziomy bledow')

hold off