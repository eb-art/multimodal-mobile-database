
\documentclass[a4paper,12pt]{report}

%\usepackage[OT4]{polski}
\usepackage[utf8]{inputenc}
\usepackage{polski}
\usepackage{array}

% Standard packages and definitions (already calls for mandatory Latex packages)
\usepackage{NASKTechRep.Layout}
\usepackage{NASKTechRep.Defs}
\ifthenelse{\equal{\NASKTRFullReportLanguage}{PL}}



% Add here your packages if needed
\usepackage{multirow}
%\usepackage{rotating}
\usepackage{color}
\usepackage{colortbl}
%\usepackage{ifthen}
%\usepackage{ams}
%\usepackage{amsfonts}
\usepackage{amsmath}
%\usepackage{amsthm}
%\usepackage{amssymb}
%\usepackage{amsbsy}
%\usepackage{amsgen}
%\usepackage{pifont}
%\usepackage{mathrsfs}
%\usepackage{shadow}
%\usepackage{comment}
\usepackage{latexsym}
%\usepackage{subfig}
\usepackage{subcaption}
\usepackage{blindtext}
\definecolor{LightPurple}{rgb}{0.88,0.8,1}


\newcommand\sectionbreak{\clearpage}
\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set proprietary (fancy-based) page style
\NASTTRMakeFancyPageStyle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% print header (with info table)
\NASTTRPrintHeader



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ABSTRACT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\def\NASKTRabstract{
(...)
}\NASTTRMakeTitle


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\noindent
{\large{\bf Przedmiot odbioru pracy:}}\\

\noindent
Przedmiotami odbioru prac wykonanych w {\bf Etapie II}  projektu \textit{,,Multimodalne systemy biometryczne na urządzenia mobilne"} są:
\begin{itemize}
\item multimodalna  baza danych zebrana za pomocą urządzeń mobilnych,
\item scenariusz testów,
\item artykuł opisujący bazę danych,
\item sprawozdanie  w postaci raportu naukowo - technicznego.
\end{itemize}

\vskip1cm
{\large
\noindent
{\bf Wykonawcy Etapu II:}}\\

\noindent
Ewelina Bartuzi [EB]\\
Radosław Białobrzeski [RB]\\
Katarzyna Roszczewska [KR]\\
Dominika Szwed [DS]\\
Mateusz Trokielewicz [MT]\\


\newpage
\chapter{Podsumowanie etapu II}



\section{Wstęp - TO DO!}



Kluczowym celem (dziełem) projektu  \textit{,,Multimodalne systemy biometryczne na urządzenia mobilne''} było stworzenie multimodalnej bazy danych, zawierającej liczne charakterystyki biometryczne pobierane za pomocą nowoczesnych telefonów komórkowych. W bazie znajdą się:
\begin{itemize}
\item \textbf{podpisy}
\item \textbf{nagrania głosu}
\item \textbf{zdjęcia oczu / tęczówki}
\item \textbf{zdjęcia twarzy} (termiczne i tradycyjne)
\item \textbf{zdjęcia dłoni} (termiczne i tradycyjne)
\end{itemize} 

Wszystkie charakterystyki zostały pobrane w trzech sesjach pomiarowych oddalonych w czasie o miesiąc. \\

Zebrana baza danych umożliwi przeprowadzanie badań nad metodami rozpoznawania tożsamości na urządzenia mobilne. Istotne było, aby proces zbierania danych biometrycznych został przemyślany i przeprowadzony zgodnie z przygotowanym scenariuszem, indywidualnie dobranym do każdej z modalności.\\

W drugim etapie projektu wykonano następujące zadania przewidziane w umowie o dzieło:
\begin{itemize}
\item{zebrano dane biometryczne za pomocą urządzeń mobilnych,}
\item{opracowano scenariusz testowania metod,}
\item{przygotowano materiał opisujący bazę danych, gotowy do publikacji w czasopiśmie z listy ministerialnej z części A.}
\end{itemize}


\newpage

\section{Wyniki}



\begin{table}[h!]
\renewcommand{\arraystretch}{1.5}
%\vskip0.05cm
\centering
\footnotesize
\begin{tabular}{ >{\raggedright\arraybackslash }m{0.35\linewidth}>{\raggedright\arraybackslash}m{0.65\linewidth}} 
\hline

\textit {Wynik:}  &  \textit {Opis:} \\

\hline
{\bf Baza danych} & powstała w wyniku pobierania próbek biometrycznych za pomocą urządzeń mobilnych, zgodnie z opracowanymi w ramach etapu pierwszego scenariuszami. pomiarowymi \linebreak Wykonanie: wszyscy,  czerwiec-wrzesień 2017\\

{\bf Utrzymanie bazy i przygotowanie danych:}& działania mające na celu bezpieczne przechowywanie danych (zgrywanie danych z urządzeń pomiarowych na serwer wewnętrzny, korekcja błędów w bazie, konwertowanie danych, przycinanie zdjęć) oraz wstępna analiza danych. \linebreak Wykonanie: wszyscy,  czerwiec-październik 2017\\

{\bf Scenariusz testowania danych:} & protokół przeprowadzania eksperymentów na bazie, zalecany do porównywania wyników opracowywanych metod. \linebreak Wykonanie: EB, KR, RB, październik 2017\\

%\hskip1cm podpis & Katarzyna Michowska, maj-czerwiec 2017\\
{\bf Artykuł:} & przygotowanie materiału gotowego do opublikowania w czasopiśmie z listy MNISW (część A), zawiera przegląd literatury, szczegółowy opis bazy (specyfikację urządzeń pomiarowych, scenariusze pobierania danych, parametry bazy), wyniki uzyskane za pomocą popularnych algorytmów wykorzystywanych w biometrii. \linebreak Wykonanie: EB, KR, RB, MT, wrzesień - październik 2017\\
{\bf Raport:} & materiał opisujący wyniki drugiego etapu Projektu \linebreak Wykonanie: EB, KR, RB, październik 2017\\
\hline
\end{tabular}
\caption{Tabela przedstawiająca wyniki pracy w etapie II z uwzględnieniem terminów i wykonawców zadania.}
\label{wyniki_projektu}
\end{table}



\newpage
\section{Zakupy}

{\bf W Etapie II nie  dokonywano zakupu środków trwałych, ani innych usług}. Wykorzystywano przedmioty zakupione podczas Etapu I. Dla jasności poniżej znajduje się tabela przedstawiająca zakupione wcześniej przedmioty.
\begin{table}[h!]
\renewcommand{\arraystretch}{1.5}
%\vskip0.05cm
\centering
\footnotesize
\begin{tabular}{ >{\raggedright\arraybackslash }m{0.3\linewidth}>{\raggedright\arraybackslash}m{0.7\linewidth}} 
\hline

\textit {Przedmiot zakupu:}  &  \textit {Cel} \\
%
\hline

\textbf{TELEFONY:}&\\

Huawei Mate S  & pobieranie podpisu wraz z informacją o nacisku w danych punktach\\
Huawei P9 Lite  & pobieranie nagrań głosu \\
CATEPILAR S60  & pobieranie charakterystyk w postaci zdjęć termicznych\\
&\\
\textbf{AKCESORIA:}&\\

zewnętrzna kamera termowizyjna do smartfonów FLIR ONE (kompatybilnej z iOS) & pobieranie charakterystyk w postaci zdjęć termicznych na urządzenia z innym system operacyjnym\\

rysik -   Adonit DASH 2  & pobieranie podpisu (punktowy styk z ekranem telefonu, symulacja podpisu wykonywanego długopisem)\\
&\\
\textbf{INNE:}&\\

komputer MacBook Air  & pobieranie  i konwertowanie danych (gł. pobranych przy pomocy kamery termowizyjnej), umożliwia tworzenie aplikacji wieloplatformowych \\
Microsoft Visual Studio 2015 Professional  & umożliwia pisanie silników biometrycznych\\
\hline
{\bf SUMA:} & 12 928,00 zł netto\\
\hline
\end{tabular}
\caption{Wykaz zakupionych środków trwałych na potrzeby Projektu.}
\label {zakupy}
\end{table}


\newpage
\section{Czas pracy i koszty realizacji - TODO sprawdzić liczbę godzin}

Etap II projektu {\it ,,Multimodalne systemy biometryczne na urządzenia mobilne''} trwał pięć miesięcy (czerwiec - październik).  Nad realizacją zadań z tej części Projektu pracowało pięć osób. Stawka godzinowa dla głównych wykonawców projektu to $35$ {\it zł /h}, natomiast dla osób pomagających w tworzeniu, utrzymaniu bazy wynosiła $35$ {\it zł /h}. Poniżej znajduje się tabela przedstawiająca godzinowy udział wykonawców w poszczególnych zadaniach. 

\begin{table}[h!]
\renewcommand{\arraystretch}{1.5}
%\vskip0.05cm
\centering
\footnotesize
\begin{tabular}{>{\raggedright\arraybackslash }m{0.35\linewidth}>{\raggedright\arraybackslash}m{0.1\linewidth}>{\raggedright\arraybackslash}m{0.1\linewidth}>{\raggedright\arraybackslash}m{0.1\linewidth}>{\raggedright\arraybackslash}m{0.1\linewidth}>{\raggedright\arraybackslash}m{0.1\linewidth}} 
\hline

\textit {Zadania}  &  \textit{RB} &  \textit{KM} &  \textit{EB} &  \textit{DS} &  \textit{MT}\\

\hline
Pobieranie danych & 10 & 10 & 10 & 275 & -\\
Utrzymanie bazy \linebreak(struktury i bezpieczeństwa) & 10 & 30 & 30 & 30 & -\\
Przygotowanie danych & - & 20 & 20& 100 & 60 \\
Praca koncepcyjna, \linebreak scenariusze testowania & 30 & 20 & 20 & -& -\\
Przygotowanie raportu & 30 & 60 & 60 & -& -\\
Przygotowanie artykułu & 100 & 110 & 110 & -& 75\\
%Sprawy związane z zakupem sprzętu &2 &2 &2 \\
%Sprawy administracyjne i prawne &3 &3 &3 \\
%Praca koncepcyjna &10 &10 &10 \\
%Przygotowanie stanowisk &5 &5 &5 \\
%Przygotowanie scenariuszy pobierania danych &10 &25 &25 \\
%Przygotowanie aplikacji pobierania danych &90 &115 &115 \\
%Przygotowanie raportu &15 &20 &20 \\
\hline
{\bf Całkowita liczba godzin:} & \textit{180 h} & \textit{240 h} & \textit{240 h} & \textit{405 h} & \textit{135 h}\\
{ Stawka godzinowa:} & \textit{35 zł/h} & \textit{35 zł/h} & \textit{35 zł/h} & \textit{30 zł/h} & \textit{30 zł/h}\\
{\bf Koszt:} & {\it 6 300 zł zł}& {\it 8 400 zł}&  {\it 8 400 zł} & {\it 12 150 zł} &  {\it 4 050 zł}\\
\hline
{\bf SUMA:}& \multicolumn{5}{c}{\textbf{\textit{39 300 zł}}}\\
\hline
\end{tabular}
\caption{Podział zadań na czas pracy i wykonawców}
\label {harmonogram}
\end{table}

\newpage
\section{Wykaz publikacji powstałych w wyniku realizacji Etapu II}

W ramach Etapu II  przygotowano materiał do opublikowania w czasopiśmie z listy A MNISW. Produkt ten zawiera:
\begin{itemize}
\item przegląd literatury,
\item opis stanowiska oraz specyfikację urządzeń pomiarowych
\item opis bazy danych i scenariuszy akwizycji próbek biometrycznych,

\item  wyniki otrzymane na danych z bazy, wykorzystujące popularne algorytmy rozpoznawania biometrycznego.
\end{itemize}

Przygotowany materiał stanowi  Załącznik 1. Planowane są kolejne publikacje dotyczące poszczególnych charakterystyk.\\
%
%
%Planowana publikacja dotycząca cząstkowych rezultatów projektu powstanie jako jeden z
%wyników kolejnych etapów Projektu.

\newpage
\chapter{Sprawozdanie merytoryczne}

\section{Baza MobiBits}

Multimodalne bazy danych biometrycznych zbierane nowoczesnymi urządzeniami mobilnymi są obecnie pożądanym produktem. Szczególnie ze względu na rosnące zainteresowanie biometrią na urządzenia przenośne typu telefon komórkowy i tablet.\\

Zebrana w ramach Projektu baza MobiBits (ang. {\it Multimodal mOBIle BIometrics daTabaSe}) jest unikatowa ze względu na bogaty scenariusz pomiarowy oraz dużą liczbę próbek biometrycznych.\\

\section{Stanowisko pomiarowe}

Dane biometryczne były pobierane przy stanowisku złożonego z:

\begin{itemize}
\item trzech telefonów komórkowych z wgranymi aplikacjami umożliwiającymi pobieranie próbek biometrycznych (przygotowanymi w ramach Etapu I):
	\begin{itemize}
	\item {\bf Huawei Mate S} - telefon służący do pobierania {\bf podpisów} oraz {\bf zdjęć oczu i dłoni} w świetle widzialnym, posiada czujnik nacisku, który umożliwia pobieranie dodatkowej charakterystyki w przypadku podpisu oraz stosunkowo dobry aparat fotograficzny (zdjęcia 13 megapikseli)
	\item {\bf Huawei P9 Lite} - telefon o niskiej cenie i  średnich parametrach (na rok 2017),  wykorzystywany do pobierania {\bf nagrań głosowych}
	\item {\bf CATEPILAR s60} - telefon wyposażony we wbudowaną kamerę termowizyjną, umożliwił pobieranie zdjęć twarzy i dłoni  w świetle widzialnym oraz rejestrować rozkłady  temperatur obiektów.
	\end{itemize}
\end{itemize}
\noindent


Telefony zostały wyposażone w aplikacje umożliwiające akwizycję danych w określonych scenariuszach pomiarowych. Szczegółowy opis programów jest zawarty w raporcie z Etapu I \cite{EtapI}

\begin{itemize}

	
\item akcesoriów pomocniczych:
	\begin{itemize}
	\item przemysłowej kamery termowizyjnej FLIR, umożliwiającej rejestrowanie temperatur z wysoką rozdzielczością 
	\item rysika Adonit Dash 2, dzięki któremu uzyskujemy punktowy nacisk na ekranie
	\item stabilizatora położenia dłoni  
	\item przedmiotów do ocieplania i chłodzenia dłoni - zimne i ciepłe okłady, suszarka do włosów oraz termofor elektryczny.
	\end{itemize}
\end{itemize}

\begin{table}[h!]
\renewcommand{\arraystretch}{1.5}
%\vskip0.05cm
\centering
\footnotesize
\begin{tabular}{>{\raggedright\arraybackslash}m{0.3\linewidth}>{\raggedright\arraybackslash}m{0.35\linewidth}>{\raggedright\arraybackslash}m{0.3\linewidth}} 
\hline

\textit {Telefon}  &  \textit{Cechy}  &  \textit{Modalności} \\

\hline

{\bf Huawei Mate S} & czujnik nacisku \linebreak duży ekran ($5.5$ cala)& podpis\\
& dobry tylny aparat ($13$ megapikseli) & zdjęcia dłoni (palmprint)\\
{\bf Huawei P9 Lite} & średnie parametry \linebreak średnia cena & nagrania głosowe\\
{\bf CAT s60} & wbudowana kamera termowizyjna (rozdzielczość $160 \times 120$ pikseli)& zdjęcia twarzy i dłoni \linebreak (zdjęcia w świetle widzialnym i termiczne)\\
\hline
\end{tabular}
\caption{Zestawienie wykorzystywanych urządzeń mobilnych do pobierania określonych modalności}
\label {Tab:telefonyModalnosci}
\end{table}

\newpage
\section{Opis bazy} %+ scenariusze

Baza danych \textit{MobiBits} jest  bazą multimodalną, w której dane pobrane zostały za pomocą telefonów komórkowych. Zawiera ona dane pobrane w trzech sesjach pomiarowych od $54$ osób. W bazie zawarte są dane pobrane od $20$ kobiet oraz $34$  mężczyzn. Sesje odbyły się w odstępach miesiąca między każdą (od [***] do [***] dni). Sesje, które złożone były z dwóch podejść pomiarowych danych modalności odbywały się z przerwą około $15$ minut.
%Przerwy pomiędzy kolejnymi sesjami pomiarowymi wynosiły od ... do ... dni (około miesiąc).\\


%%%[***] Szczegółowy opis danych i sposób ich pobierania został przedstawiony w kolejnych punktach: [***]\\
%%%Baza danych zawiera następujące dane: nagrania głosu, podpisy odręczne, zdjęcia oczu/ tęczówek, zdjęcia twarzy oraz zdjęcia dłoni. Poniżej znajdują się wizualizacje przykładowych danych dla każdej modalności.
%%%
%%%
%%%
%%%
%%%\textcolor{red}{\textbf{Nie wiem czy tego nie połączyć ze scenariuszami? żeby był opis z obrazkami}}


%\begin{itemize}
%\item {\bf nagrania głosowe}
\subsection*{NAGRANIA GŁOSOWE}

W bazie znajdują się nagrania mowy swobodnej i czytanej. Pomiar odbył się w {\bf czterech} sesjach:
\begin{itemize}
\item {\bf sesja 1. {\it swobodna} } - nagrania {\it mowy swobodnej} o różnej długości, każda z osób udzielała ośmiu odpowiedzi na wcześniej przedstawione pytania (Załącznik 5. GŁOS - sesja 1 -karta pytań)

\item {\bf sesja 2. {\it czytelnicza}} - nagrania {\it tekstu czytanego}, sesja polegała na odczytywaniu $4-5$ fragmentów baśni pt. {\it Dziewczynka z zapałkami}, od 15 do 30 sekund (Załącznik 6. GŁOS - sesja 2 - fragmenty baśni {\it Dziewczynka z zapałkami})

\item {\bf sesja 3. {\it rejestracyjna}} - nagrania przedstawiające odpowiedzi na pięć pytań dotyczących przyjętej fałszywej tożsamości - imię i nazwisko, numer karty kredytowej, adres zamieszkania, numer PESEL  (Załącznik 7. GŁOS - sesja 3 - fałszywe tożsamości)

\item {\bf sesja 4. {\it testowa}} - nagrania mowy swobodnej, będące odpowiedziami na osiem pytań o znanej  treści - od 4 do 12 sekund (Załącznik 8. GŁOS - sesja 4 - karta pytań)
\end{itemize}

%Poniżej znajduje się wizualizacja nagrania głosowego: przebiegu w czasie oraz spektrogram.

\begin{figure}[h!]
\centering
\includegraphics[width=1\linewidth]{./graphics/voice.png}
\caption{Wizualizacja nagrania głosu jednej osoby. {\bf Górny wykres:} przebieg w czasie. {\bf Dolny wykres:} spektrogram.}
\label{voice_images}
\end{figure}

%\newpage
%\item {\bf podpisy {\it on-line}}
\subsection*{ PODPISY \textbf{\textit{ ON-LINE}}}

Baza zawiera podpisy odręczne rejestrowane w czasie rzeczywistym. Znana jest kolejność punktów w czasie (częstotliwość zbierania danych) reprezentowanych poprzez odczytywane w kolejnych punktach  czasowych:
\begin{itemize}
\item wartości na osi $X$
\item wartości na osi $Y$
\item wartości nacisku na powierzchnię wyświetlacza w danym punkcie $(x,y)$
\end{itemize}

Dane zostały zebrane w czterech  sesjach, podczas których każda z osób złożyła po 10 podpisów. Dodatkowo dla każdej z osób wykonanych zostało 20 fałszerstw zaawansowanych ($5$ podpisów $\times$ $4$ fałszerzy). Jest to istotne z punktu badania metod na próby uzyskania nieautoryzowanego dostępu. \\
\noindent
{\it(Opracowane scenariusze w ramach  Etapu I zakładały  zebranie po $10$ podpisów własnych po około $15$ minutach od pierwszego pomiaru w sesji $2.$ i $3.$ oraz po $40$ fałszerstw zaawansowanych. Zrezygnowano z ich wykonania ze względu na zbyt dużą liczbę podpisów do złożenia, co było uciążliwe dla osób zarejestrowanych w bazie danych.)} 

\begin{figure}[h!]
\centering
\includegraphics[width=1\linewidth]{./graphics/podpisy.png}
\caption{Wizualizacja podpisu jednej osoby - wykresy dwóch parametrów $y(x)$.}
\label{signature_images}
\end{figure}

%\newpage
%
%\item {\bf zdjęcia oczu/tęczówek}

\subsection*{OCZY / TĘCZÓWKI}

Zdjęcia oczu zostały zebrane w świetle widzialnym za pomocą telefonu Huawei Mate S. Akwizycja danych została podzielona na trzy sesje pomiarowe:

\begin{figure}[h!]
\centering
\includegraphics[width=0.98\linewidth]{./graphics/RadeksEyes_ozn.png}
\caption{Zdjęcia tęczówek pobrane od jednej osoby w kolejnych sesjach pomiarowych.}
\label{eye_images}
\end{figure}

\noindent
{\it (Podczas sesji 3. planowo miały być zebrane zdjęcia oczu przednim aparatem telefonu komórkowego. Ze względu na niską jakość zdjęć (rozdzielczość 8 megapikseli) zrezygnowano z tej prezentacji na rzecz zbadania jakości obrazu tęczówki, który można wydobyć ze zdjęć całej twarzy.)}



%\item {\bf zdjęcia twarzy}
\subsection*{TWARZ}

Zdjęcia twarzy zostały pobrane od [***] osób w trzech sesjach pomiarowych za pomocą telefonu CAT s60 z wbudowaną kamerą termowizyjną. Rejestrowano twarz w sześciu pozycjach, uzyskując zdjęcia: {\bf frontalne}, {\bf półprofilowe lewe}, {\bf  półprofilowe prawe}, {\bf  profilowe lewe}, {\bf  profilowe prawe} oraz zdjęcie typu \textbf{\textit{selfie}}. Każde ustawienie twarzy zostało sfotografowane przynajmniej dwukrotnie. Zdjęcia twarzy zostały wykonane w świetle widzialnym oraz posiadają odpowiadającą im macierz temperatur, przedstawiającą rozkład ciepła twarzy.

\begin{figure}[h!]
\centering
\includegraphics[width=1\linewidth]{./graphics/faces.png}
\caption{Zdjęcie twarzy jednej osoby pobrane dla różnych ustawień twarzy. {\bf Pierwszy rząd:} zdjęcia w świetle widzialnym. {\bf Drugi rząd:} zdjęcie termiczne (interpretacja kolorystyczna)}
\label{face_images}
\end{figure}


%\item {\bf zdjęcia dłoni}
\subsection*{DŁOŃ}

Zdjęcia dłoni zostały pobrane w trzech sesjach wykorzystując telefon komórkowy CAT s60 wyposażony w sensor promieniowania cieplnego (zdjęcia termiczne oraz w świetle widzialnym) oraz dodatkowej sesji, uwieczniającej dłoń aparatem Huawei Mate S o dużej rozdzielczości (13 megapikseli, zdjęcia w świetle widzialnym):

\begin{itemize}
\item {sesja 1.} po 3 zdjęcia strony wewnętrznej i zewnętrznej każdej dłoni, zdjęcia
	\begin{itemize}
	\item [a.] dłoń położona na szklanej podstawce 
	\item [b.] dłoń w niewymuszonej pozycji 
	\end{itemize}
	
	\item {sesja 2.} po 3 zdjęcia strony wewnętrznej i zewnętrznej każdej dłoni stabilizowanej na płytce, 
	\begin{itemize}
	\item [a.] dynamiczne zmiany pod wpływem chłodzenia dłoni kompresem
	\item [b.] dynamiczne zmiany pod wpływem grzania dłoni kompresem
	\end{itemize}
	
	\item {sesja 3.} po 3 zdjęcia strony wewnętrznej i zewnętrznej każdej dłoni w pozycji nie wymuszającej, 
	\begin{itemize}
	\item [a.] dynamiczne zmiany pod wpływem chłodzenia dłoni kompresem
	\item [b.] dynamiczne zmiany pod wpływem grzania dłoni kompresem
	\end{itemize}
	
	\item {sesja dodatkowa} po 5 zdjęć strony wewnętrznej każdej dłoni w pozycji nie wymuszającej, 
	\begin{itemize}
	\item [a.] z lampą błyskową
	\item [b.] bez lampy
	\end{itemize}
\end{itemize}
\vskip-0.3cm

\begin{figure}[!htp]
\centering
\includegraphics[width=1\linewidth]{./graphics/Radek_hands_part1.png}

\end{figure}

\newpage
\begin{figure}[!htp]
\centering
\includegraphics[width=1\linewidth]{./graphics/Radek_hands_part2.png}
\caption{Zdjęcie stanowiska do pobierania danych.}
\label{hand_images}
\end{figure}
%\end{itemize}


\newpage
\subsection*{PODSUMOWANIE}
Ze względu na to, że baza zawiera wiele modalności biometrycznych nie każdy z ochotników zgodził się na oddanie próbek każdej z nich. Dodatkowo niektóre osoby nie mogły się zjawić na każdej z organizowanych sesji pomiarowych. Z tego względu kompletność zebranych danych jest uzależniona od modalności i sesji. Poniższa tabela przedstawia stopień kompletności zebranych danych dla każdej ze zbieranych modalności.\\

\begin{table}[htp!]
\renewcommand{\arraystretch}{1.5}
%\vskip0.05cm
\centering
\footnotesize
\begin{tabular}{>{\raggedright\arraybackslash}m{0.12\linewidth}>{\raggedright\arraybackslash}m{0.2\linewidth}>{\raggedright\arraybackslash}m{0.2\linewidth}>{\raggedright\arraybackslash}m{0.2\linewidth}>{\raggedright\arraybackslash}m{0.2\linewidth}} 
\hline

\textit {Modalność}  &  \textit{Kompletne dane} &  \textit{Brak zgody} &  \textit{ Niekompletne sesje } &  \textit{ Niekompletne dane } \\

\hline
{\bf nagrania głosu} & 45 (do zrobienia 3) & 1 & 5 & 0 \\
{\bf podpis odręczny} & 36 (do zrobienia 13) & 1 & 4 & 0\\
{\bf zdjęcia oczu/ tęczówek} & 38 (do zrobienia 4) & 1 & 10 & 1\\
{\bf zdjęcia twarzy} & 41 (do zebrania 3) & 1 & 4 & 5\\
{\bf termiczne zdjęcia dłoni} & ? & 0 & ? & ?\\
{\bf wizualne zdjęcia dłoni} & 46 (do zebrania 5) & 0 & 2 & 1\\
\hline
\end{tabular}
\caption{Zestawienie zgromadzonych danych}
\label {stats}
\end{table}

%PODSUMOWANIE - PORÓWNANIE

Odnotowano istnienie trzech artykułów naukowych opisujących multimodalne bazy biometryczne pobieranych urządzeniami mobilnymi. Są to bazy:
\begin{itemize}
\item  \textbf{MobBIO}  \cite{MobBio_db} - zawiera dane zebrane od \textbf{105 osób} za pomocą urządzenia przenośnego typu \textbf{tablet} - Asus Transformer Pad TF 300. Odbyła się \textbf{jedna sesja pomiarowa}, w ramach której pozyskano trzy z najpopularniejszych modalności biometrycznych:
\begin{itemize}
	\item obrazy \textbf{tęczówek} (8 zdjęć dla każdego oka),
	\item zdjęcia \textbf{twarzy} (po 16 zdjęć), 
	\item \textbf{nagrania głosu} w postaci czytanych zdań trwających po 10 sekund (po 16 próbek).
\end{itemize}

\item  \textbf{MBMA} - opisana w artykule {\it ,,Multi-modal biometrics for mobile authentication''} \cite{MMB_Arnowitx_db} zawiera dane pobrane od \textbf{100 osób} za pomocą dwóch telefonów i dwóch tabletów z różnymi systemami operacyjnymi (telefony iPhone 4s i Galaxy S2, tablety iPad 2 i Motorola Xoom) w  \textbf{dwóch sesjach pomiarowych}.  W tym przypadku zebrano również trzy charakterystyki biometryczne znajdujące się czołówce pod względem popularności:
\begin{itemize}
	\item zdjęcia \textbf{twarzy} (po 3 zdjęcia),
	\item \textbf{nagrania głosowe} (4 próbki),
	\item \textbf{podpisy} (8 próbek danej osoby).
\end{itemize}
\noindent


\item  \textbf{BioSecure}  \cite{BioSecure_db}  - zawiera ona dane pobrane od \textbf{713 osób}, których akwizycja odbyła się w \textbf{dwóch sesjach pomiarowych} (odstęp czasu pomiędzy sesjami wynosił od 1 do 3 miesięcy). Dane w przypadku tej bazy pobierane były na różnych uniwersytetach, za pomocą dwóch urządzeń: Samsung Q1 + Philips SPC900NC Webcam, HP iPAQ  hx2790 PDA. W bazie zawarte są cztery charakterystyki biometryczne: 

\begin{itemize}
	\item 4 frontalne zdjęcia \textbf{twarzy} (2 na zewnątrz przy świetle naturalnym i 2 w pomieszczeniu - sztuczne oświetlenie),
	\item 12 próbek \textbf{odcisków palców},
	\item 18 \textbf{nagrań głosowych} (9 na zewnątrz i 9 w pomieszczeniu),
	\item 25 \textbf{podpisów odręcznych} (15 podpisów własnych i 10 fałszerstw).
\end{itemize}


\end{itemize}



\begin{table}[h!]
\renewcommand{\arraystretch}{1.5}
%\vskip0.05cm
\centering
\footnotesize
\begin{tabular}{>{\raggedright\arraybackslash}m{0.12\linewidth}>{\raggedright\arraybackslash}m{0.2\linewidth}>{\raggedright\arraybackslash}m{0.2\linewidth}>{\raggedright\arraybackslash}m{0.2\linewidth}>{\raggedright\arraybackslash}m{0.2\linewidth}} 
\hline

\textit {}  &  \textit{MobiBits} &  \textit{MobBIO} &  \textit{MBfMA} &  \textit{BioSecure} \\

\hline
{\bf liczba osób} & 54 & 105 & 100 & 713\\
{\bf liczba sesja} & 3 & 1 & 2 & 2\\
{\bf urządzenia pomiarowe} & Huawei Mate S \linebreak Huawei P9 Lite \linebreak CAT S60& Asus Transformer Pad TF 300& iPhone 4s \linebreak Galaxy S2 \linebreak iPad 2 \linebreak Motorola Xoom & Samsung Q1 \linebreak Philips SPC900NC Webcam \linebreak HP iPAQ hx2790 PDA\\
{\bf modalności} & {\bf podpis}  \linebreak {\bf nagrania głosowe}  \linebreak {\bf zdjęcia twarzy}   \linebreak {\bf zdjęcia oczu} \linebreak {\bf zdjęcia dłoni}  & {\bf nagrania głosowe} \linebreak {\bf zdjęcia twarzy} \linebreak {\bf zdjęcia oczu} & {\bf nagrania głosowe} \linebreak {\bf podpisy} \linebreak {\bf zdjęcia twarzy}  & {\bf nagrania głosowe} \linebreak {\bf podpisy} \linebreak {\bf zdjęcia twarzy} \linebreak {\bf odciski palców } \\
%{\bf modalności} & {\bf podpisy} \linebreak ($30 \times$ oryginalne, $15\times$fałszerstwa zaawansowane) \linebreak {\bf nagrania głosowe} \linebreak ($18\times$ mowa swobodna, $16\times$ mowa czytelnicza, ) \linebreak {\bf twarz}  \linebreak ($12\times$ profile, $12\times$ półprofile, $6\times$ zdjęcia frontalne, $6\times$ {\it selfie}) \linebreak tęczówka \linebreak (34 zdjęcia każdego oka, 24 okolic oczu)) \linebreak dłoń \linebreak (zdjęcia lewej i prawej dłoni, wnętrza i części zewnętrznej, ) & głos \linebreak twarz \linebreak tęczówka &&\\
\hline
\end{tabular}
\caption{Porównanie multimodalnych baz danych pobieranych urządzeniami mobilnymi}
\label {harmonogram}
\end{table}

%\section{Dotychczasowe prace dotyczące  multimodalnych, mobilnych baz danych}
%%\input{chapters/related}






Zaprezentowane bazy chociaż posiadają dane od sporej liczby osób zawierają również wady - proste scenariusze pobierania danych, mała liczba sesji pomiarowych. Problemem jest również fakt, iż są stosunkowo ,,stare", biorąc pod uwagę rozwój technologii związanych z urządzeniami mobilnymi w ostatnich latach. Nowoczesne telefony posiadają aparaty o dużej rozdzielczości, co zwiększa jakość próbek w postaci zdjęć tęczówek, twarzy i dłoni,  sensory nacisku, które pozwalają wprowadzić dodatkowe dane do wzorca podpisu zwiększając bezpieczeństwo metody. Ostatnio pojawiły się kamery termowizyjne kompatybilne z urządzeniami mobilnymi z systemem Android lub iOS oraz telefon komórkowy z wbudowaną kamerą termowizyjną FLIR ONE, które  umożliwiają wprowadzenie systemów biometrycznych na urządzenia przenośne wykorzystujących dodatkowo termikę w systemach multimodalnych lub działającą, jako test żywotności odpierający próby podszycia się.%jaka jest miara opisująca 


\newpage
\section{Scenariusz testowania}

Porównanie danych
Fuzja wyników

Wyniki - scenariusz weryfikacji - ROC
Wyniki - scenariusz identyfikacji - CMC

\newpage
\begin{table}[h!]
\renewcommand{\arraystretch}{1.5}
%\vskip0.05cm
\centering
\footnotesize
\begin{tabular}{>{\raggedright\arraybackslash}m{0.18\linewidth}>{\raggedright\arraybackslash}m{0.18\linewidth}>{\raggedright\arraybackslash}m{0.18\linewidth}>{\raggedright\arraybackslash}m{0.18\linewidth}>{\raggedright\arraybackslash}m{0.18\linewidth}} 
\hline

\textit {}  &  {\textbf{SESJA 1.}} &  \textbf{SESJA 2.} &  \textbf{SESJA 3.} &  \textbf{SESJA 4./ \linebreak  DODATKOWA} \\


\hline
\end{tabular}
\caption{odstępy czasowe w sesjach}
\label {harmonogram}
\end{table}



\begin{table}[h!]
\renewcommand{\arraystretch}{1.5}
%\vskip0.05cm
\centering
\footnotesize
\begin{tabular}{>{\raggedright\arraybackslash}m{0.12\linewidth}>{\raggedright\arraybackslash}m{0.2\linewidth}>{\raggedright\arraybackslash}m{0.2\linewidth}>{\raggedright\arraybackslash}m{0.2\linewidth}>{\raggedright\arraybackslash}m{0.2\linewidth}} 
\hline

\textit {}  &  \textit{MobiBits} &  \textit{MobBIO} &  \textit{MBfMA} &  \textit{BioSecure} \\


\hline
\end{tabular}
\caption{urządzenia pomiarowe w modalnościach}
\label {harmonogram}
\end{table}



\newpage




%\chapter{Opis bazy danych}
%\input{chapters/database}
%\label{chapter_database}







\newpage
\section{Scenariusze pobierania danych}

Przed zbieraniem bazy istotny etap stanowi opracowanie scenariuszy pomiarów wybranych modalności biometrycznych. Każda z charakterystyk posiada inne właściwości, wady i zalety, a także zależności od warunków zewnętrznych. Scenariusze pomiarowe, opracowane w ramach Etapu I zostały napisane tak, aby odpowiednio uwzględniały czynniki losowe (szczególnie ważne w przypadku podpisu i termiki) oraz symulowały realne zastosowania (bardzo istotne w przypadku głosu - nagrania testowe zwykle różnią się charakterem od nagrań rejestracyjnych). \\

Zaproponowane w Etapie I scenariusze pobierania danych musiały zostać delikatnie zmodyfikowane ze względu na obserwacje poczynione podczas zbierania danych. Poniżej znajduje się opis rzeczywistych scenariuszy pobierania danych dla poszczególnych modalności. 
	
\subsection*{Podpis}
	
W bazie danych $MobiBits$ znajdują się trzy charakterystyki podpisu odręcznego: \textbf{przebieg wzdłuż osi X, przebieg wzdłuż osi Y, nacisk na powierzchnie wyświetlacza w danym punkcie}. Dodatkowo, ze względu na zbieranie podpisu w czasie rzeczywistym znana jest \textbf{kolejność punktów w czasie}, co oznacza, że zebrane podpisy są to realizacje on-line. Wykonane zostały \textbf{3 sesje pomiarowe}:

\begin{itemize}
	\item \textbf{sesja 1.} - po $10$ podpisów własnych dla każdej z osób.
	\item \textbf{sesja 2.} - $15-20$ minut po sesji pierwszej; po 10 podpisów własnych dla każdej z osób.
	\item \textbf{sesja 3.} - po miesiącu od sesji pierwszej; po $10$ podpisów własnych dla każdej z osób.
	\item \textbf{sesja 5.} - po dwóch miesiącach od sesji pierwszej; po $10$ podpisów własnych dla każdej z osób.
\end{itemize}

W przypadku podpisu odręcznego on-line, kluczowe jest zbadanie jego odporności na fałszerstwa zaawansowane. Z tego względu każda z osób została poproszona o wykonanie \textbf{5 fałszerstw zaawansowanych dla 4 kolejnych osób z bazy}. W ramach sesji $3.$ i $5.$ każdy ochotnik próbował podszyć się pod dwie wybrane osoby. Początkowo planowane było wykonanie sesji $4.$ i $6.$ Zakładały one zebranie po $10$ podpisów własnych po około $15$ minutach od sesji $3.$ i $4.$ Zrezygnowano z ich wykonania ze względu na zbyt dużą liczbę podpisów do złożenia, co było uciążliwe dla osób zarejestrowanych w bazie danych. Z tego samego powodu zmniejszona została liczba fałszerstw zaawansowanych.  
	
	\subsection*{Głos}

W bazie danych będą znajdują się zarówno nagrania mowy swobodnej, jak i tej o ustalonej treści. Wykonane zostały 4 sesje nagraniowe:
\begin{itemize}
\item{\textbf{sesja 1.} - \emph{swobodna} - różnej długości swobodne odpowiedzi na pytania o znanej wcześniej treści, po 8 nagrań na osobę. }
\item{\textbf{sesja 2.} - \emph{czytelnicza} - odczytywane fragmenty baśni ,,Dziewczynka z zapałkami'' (15 do 30 sekund), po 4 do 5 nagrań na osobę.}
\item{\textbf{sesja 3.} - \emph{rejestracyjna} - odpowiedzi na pytania dotyczące przyjętej fałszywej tożsamości (imię i nazwisko, numer karty kredytowej itd.), po 5 nagrań na osobę.}
\item{\textbf{sesja 4.} - \emph{testowa} - różnej długości swobodne odpowiedzi na pytania o znanej wcześniej treści (sugerowane 4 do 12 sekund), po 8 nagrań na osobę.}
\end{itemize}
Wstępnie zakładano, że w sesjach 1 i 2 oraz 3 i 4 będą uczestniczyć różne osoby ze względu na podział bazy na zbiory uczący i testowy. Zrezygnowano z tego założenia z powodu stosunkowo niewielkiej liczby osób, jednak zgromadzona ilość danych (około 5 godzin nagrań) z pewnością pozwoli na efektywne uczenie modeli probabilistycznych o znacznej liczbie parametrów oraz późniejsze testowanie tych modeli na wydzielonym zbiorze testowym.
	
	\subsection*{Dłoń}

W bazie  znajdują się zdjęcia wykonane w świetle widzialnym oraz zdjęcia termiczne dłoni. Zarejestrowano  wewnętrzne i grzbietowe strony obu dłoni ochotników za pomocą telefonu komórkowego CAT s60 z wbudowaną kamerą termowizyjną. Aplikacja na urządzenie mobilne umożliwiła pobranie zdjęć termicznych, w technologii MSX oraz zdjęć wykonanych w świetle widzialnym  z dodatkową warstwą zawierającą wartości temperatur dłoni: RGB + T. Sesje będą dzieliły się na: 

\begin{itemize}

\item statyczne - w warunkach laboratoryjnych (w pomieszczeniu z klimatyzacją ustawioną na $20^\circ$C),
\item dynamiczne - uwzględniające zmiany rozkładu temperatur pod wpływem grzania dłoni oraz ochładzania za pomocą zimnych i ciepłych okładów.
\end{itemize}

\noindent
oraz

\begin{itemize}
\item ze stabilizacją położenia dłoni,
\item ze swobodnym ułożeniem dłoni.

\end{itemize}
	
	\noindent
Odbyły się trzy sesje pomiarowe:
\begin{itemize}
	\item \textbf{sesja 1.} - po pięć prezentacji każdej strony każdej dłoni przy stanowisku ze stabilizacją.
	\item \textbf{sesja 2.} - 15-20 minut po sesji pierwszej; po pięć prezentacji dynamicznych każdej strony, każdej dłoni przy stanowisku ze stabilizacją.
	\item \textbf{sesja 3.} - po miesiącu od sesji pierwszej; 3 prezentacje tradycyjne
	\item \textbf{sesja 4.} - natychmiast po sesji 3; 3 prezentacje dynamiczne
	\item \textbf{sesja 5.} - po dwóch miesiącach od sesji pierwszej; 5 prezentacji swobodnych
	\item \textbf{sesja 6.} - natychmiast po sesji 4; 5 prezentacji swobodnych - dynamicznych
\end{itemize}


W związku z faktem, iż termika jest nową modalnością zdjęcia termiczne będą również pobierane za pomocą przemysłowej kamery termowizyjnej FLIR.\\
	
	
	\subsection*{Tęczówka}

W bazie danych $MobiBits$ zebrane zostały zdjęcia oczu w świetle widzialnym. Akwizycja danych podzielona została na $3$ sesje pomiarowe:
	
	\begin{itemize}
	\item \textbf{sesja 1.} - $5$ zdjęć każdego oka wykonanych  tylnym aparatem telefonu komórkowego z lampą błyskową. Odległość oka od aparatu wynosiła około $10$ $cm$.
	\item \textbf{sesja 2.} - $5$ zdjęć oczu wykonanych tylnym aparatem telefonu komórkowego bez wykorzystania lampy błyskowej. Odległość twarzy od aparatu wynosiła około $20$ $cm$.
	\item \textbf{sesja 3.} -  po $3$ zdjęcia oczu wykonane tylnym aparatem telefonu komórkowego dla $3$ ustawień aparatu (frontalnie w odległości około $20$ $cm$ od twarzy, frontalnie w odległości około $30$ $cm$ od twarzy oraz z góry w odległości około $30$ $cm$ od twarzy). Zdjęcia wykonano zarówno z lampą błyskową, jak i bez jej wykorzystania. Łącznie w ramach tej sesji wykonano $18$ zdjęć oczu. 
	\end{itemize}
	
W ramach sesji $3.$  planowano wykonanie zdjęć oczu przednim aparatem telefonu komórkowego. Jednak ze względu na stosunkowo niską rozdzielczość aparatu($8$ $Mpx$) zrezygnowano z tego scenariusza na rzecz zbadania jakości obrazu tęczówki, który można wydobyć ze zdjęć całej twarzy.
	
	\subsection*{Twarz}

W bazie znajdują się zdjęcia twarzy w świetle widzialnym oraz powiązane z nimi dane dotyczące rozkładu temperatur. Aplikacja na urządzenie mobilne umożliwia pobierania zdjęć termicznych, w technologii MSX oraz zdjęć wykonanych w świetle widzialnym.\\

\begin{itemize}
	\item \textbf{sesja 1.} - pięć rodzajów zdjęć twarzy: zdjęcie frontalne, dwa półprofile, dwa profile, zdjęcia typu selfie.
	\item \textbf{sesja 2.} - pięć rodzajów zdjęć twarzy: zdjęcie frontalne, dwa półprofile, dwa profile, zdjęcia typu selfie.
	\item \textbf{sesja 3.} - pięć rodzajów zdjęć twarzy: zdjęcie frontalne, dwa półprofile, dwa profile, zdjęcia typu selfie.
	\end{itemize}

\subsection*{Struktura bazy danych}

Dane są zapisywane z nazwą w formacie:
\begin{center}
\textbf{Typ\_danych\_uXXX\_sYY\_dodatkowe\_dane},
\end{center}
gdzie  \emph{Typ\_danych} to informacja opcjonalna, która określa rodzaj modalności, jakiej dane dotyczą, jeśli nie precyzuje tego rozszerzenie pliku. \emph{XXX} to trzycyfrowy numer użytkownika (z zerami wiodącymi, np. $001$), \emph{YY} to numer sesji (z zerem wiodącym), a \emph{dodatkowe\_dane} to identyfikator danych (np. numer nagrania, typ realizacji podpisu) lub data akwizycji danych.

\newpage
\section{Stanowisko do pobierania danych}
Skompletowane stanowisko do pobierania danych składa się z:
\begin{itemize}
\item kamery FLIR (termika) ze statywem będącej na wyposażeniu Zakładu Biometrii,
\item smartfonów z aplikacjami do akwizycji danych,
\item rysika Adonit Dash 2,
\item stabilizatora położenia dłoni.
\end{itemize}
Dodatkowo w skład stanowiska wchodzi komputer przenośny MacBook Air z zainstalowanymi systemami operacyjnymi Mac OS i Windows. Komputer służyć będzie do pobierania i konwersji danych, weryfikacji pobranych danych i utrzymania bieżącej dokumentacji projektu.

\begin{figure}[h!]
\centering
\includegraphics[width=0.7\linewidth]{./graphics/stanowisko}
\caption{Zdjęcie stanowiska do pobierania danych.}
\label{stanowisko}
\end{figure}

\begin{thebibliography}{99}

\bibitem{EtapI} E. Bartuzi, R. Białobrzeski, K. Michowska, raport wewnętrzny NASK ,,Multimodalne systemy biometryczne na urządzenia mobilne - przygotowania'', Warszawa, Lipiec 2017

\bibitem{thermalThesis} E. Bartuzi, Rozpoznawanie tożsamości przy wykorzystaniu obrazów termicznych dłoni, Warsaw University of Technology (2012)

\bibitem{MobBio_db} A. Sequeira i in., MobBIO:A Multimodal Database Captured with a Portable Handheld Device, 
Portugalia, 2014)

\bibitem{MMB_Arnowitx_db} H. Aronowitz i in., Multi-Modal Biometrics for Mobile Authentication,  2014

\bibitem{BioSecure_db} Javier Ortega-Garcia i in., The Multi-Scenario Multi-Environment BioSecure Multimodal Database (BMDB),  2009

\bibitem{Pressure} Test Report Force Sensing Testing With Optofidelity Goldfinger, \url{http://www.optofidelity.com/files/uploads/2016/10/OptoFidelity_GoldFinger_Force-Sensing_testreport.pdf}

\end{thebibliography}


\appendix





\end{document}