\documentclass[10pt,twocolumn,letterpaper]{article}

\usepackage{btas}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{gensymb}
\usepackage{url}


\usepackage{array}
\usepackage{multirow}
\usepackage{textcomp}
\usepackage{array}
\usepackage{xcolor,colortbl}
\graphicspath{{./graphics/}}

% \btasfinalcopy % *** Uncomment this line for the final submission

\def\btasPaperID{paperID} % *** Enter the btas Paper ID here
\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

% Pages are numbered in submission mode, and unnumbered in camera-ready
\ifbtasfinal\pagestyle{empty}\fi
\begin{document}


\title{MobiBits: Multimodal Mobile Biometric Database}

\author{Ewelina Bartuzi$^{\dag,\ddag}$, Radosław Białobrzeski$^{\dag,\ddag}$, Katarzyna Roszczewska$^{\dag,\ddag}$, Mateusz Trokielewicz$^{\dag,\ddag}$\\
$^{\dag}$Biometrics Laboratory\\
Research and Academic Computer Network\\
Kolska 12, 02-796 Warsaw, Poland\\
{\tt\small {ewelina.bartuzi, radoslaw.bialobrzeski, katarzyna.michowska,mateusz.trokielewicz}@nask.pl}
}
\maketitle
\thispagestyle{empty}

\begin{abstract}
This paper presents a novel database comprising representations of five different biometric characteristics, collected in a mobile, unconstrained or semi-constrained setting with three different mobile devices, including characteristics previously unavailable in existing datasets, namely hand images, thermal hand images, and thermal face images, all acquired with a mobile, off-the-shelf device. This collection of data is accompanied by an extensive set of experiments providing insight on benchmark recognition performance that can be achieved with these data, carried out with existing commercial and academic biometric solutions. In addition, score fusion methods are explored, proving this to be a way of improving recognition reliability and accuracy, making mobile biometrics a feasible and convenient way of daily personal authentication.
\end{abstract}


\section{Introduction}
\label{sec:Intro}

Putting emphasis on the implementation of biometric authentication on mobile devices in the recent years has lead to an almost universally ubiquitous presence of biometrics in our daily lives. Accelerating advancements in this field started with the fingerprint sensor of the iPhone 5s back in 2013 \cite{iPhone5s}. Since then, more than 700 mobile devices have supported this kind of authentication, with increasing number of manufacturers employing other biometric characteristics, such as iris in Samsung Galaxy S8, Microsoft Lumia 950, or Fujitsu Arrows \cite{GalaxyS8, LumiaIris950, FujitsuArrows}, and recently also face in 3D with Apple iPhone X \cite{FaceID}. Today, biometric authentication is used in these devices not only to unlock the phone or tablet, but also as a security method protecting our financial assets in mobile payment systems, such as Apple Pay, Android Pay, Samsung Pay \cite{ApplePay, AndroidPay, SamsungPay}.

%Apple Pay alone constitutes over 90\% of contactless payments in the US, and thus can be considered a wide-scale implementation of biometrics in finance (as of August 2017). 

Some countries are even embracing mobile-based documents as a way of providing an identity documents for their citizens, with a notable example of Estonia and its e-identity implementation \cite{EstoniaEIdentity}. More than 98\% of Estonian citizens are enrolled and actively using their e-identity for online signing, bank login, abroad travel, health insurance and prescriptions, medical records, voting, tax claims, and others. With this reliance upon mobile identity, secure and convenient authentication methods are crucial, hence the recently increasing interest in biometric authentication for mobile applications, and new research challenges:

\textit{'What other, novel biometric characteristics can be employed for mobile authentication?'}

\textit{'Can a fusion of different characteristics improve security of authentication solutions on mobile?'}

These two cardinal questions drive the motivation of this paper, whose main contribution is a novel database: {\it MobiBits}, comprising representations of five different biometric characteristics, acquired with sensors embedded in commercially available, consumer-grade mobile phones. A notable addition to the acquisition setup is the use of thermal imaging enabled smartphone, adding a new dimension to typical, visible spectrum images. According to our knowledge, our dataset offers the largest number of biometric characteristics represented in a single database, and employs the richest collection of data acquisition scenarios, which are trying to mimic those of potential real-world applications. This collection of data is then accompanied by experiments employing commercially available and academic biometric methods, which provide benchmark results of recognition performance for the introduced datasets. The {\it MobiBits} database is made publicly available to the research community at no cost for non-commercial purposes.

This paper is organized as follows: Section \ref{sec:Related} brings a review of past work related to mobile biometrics and multimodal biometric databases collected with mobile devices. Section \ref{sec:Database} details our multimodal database and the acquisition protocol. Benchmark performance and score fusion concepts are described in Section \ref{sec:Evaluation}, while Section \ref{sec:Results} presents results, and conclusions are drawn in Section \ref{sec:Conclusions}.

\section{Related work}
\label{sec:Related} 

\subsection{Iris}
Iris recognition, usually carried out with specialized devices operating in near infrared (NIR), poses several important challenges when implemented on mobile devices, such as phones and tablets -- most importantly, the need to utilize visible light images acquired with these devices. Raja \etal~investigated visible spectrum iris recognition on mobile devices with the approaches utilizing deep sparse filtering \cite{KiranSparse2014} and K-means clustering \cite{KiranKmeans}, being able to achieve EER=0.31\%. The feasibility of face and iris biometrics implementations on mobile was also studied by De Marsico \etal~\cite{FIRMEfaceIrisApp2014}. Recent research by Trokielewicz \etal~shows that visible spectrum iris recognition with high quality images acquired with an iPhone 5s is able to achieve high accuracy rates when existing commercial and academic iris matchers are employed, with FTE as low as 0\%, and EER as low as 1.19\% \cite{TrokielewiczVisibleWilga2015}. A dataset of high quality iris images acquired with a mobile phone with flash is introduced by Trokielewicz in  \cite{TrokielewiczVisibleISBA2016}. Trokielewicz and Bartuzi studied cross-spectral iris recognition, with probe samples obtained using a mobile phone matched against typical, NIR images collected with a specialized iris camera, getting EER$\simeq2\%$ \cite{TrokielewiczBartuziJTIT}. Grayscale conversion of color images with RGB channel selection dependent on the eye color is shown to improve the recognition accuracy in some cases. 

\subsection{Handwritten signatures}
Thanks to advantages such as ease of use, familiarity, and social acceptability, signature biometrics has the potential to be used on mobile devices. In the case of smartphone, it is possible to acquire handwritten signatures in the form of X and Y coordinates in time. Some phones are already equipped with a pressure sensor, which allows to record additional parameters of a signature, such as Apple iPhone 6s, 7, 8, and X, Huawei Mate S Premium, ZTE Axon Mini, Huawei P9 Plus and Meizu Pro 6. Biometric recognition methods based on {\it on-line} signatures can be divided into two groups, namely nonparametric methods and parametric methods. The most popular method from the first group is Dynamic Time Warping (DTW) \cite{DTWsignatureFaundez, DTW_Hidden_PutzPacut,  DTW_Hidden_PutzKudelski, DTWsignatureBlanco, DTWsignatureBlanco2, DTW_Putz2006}, which normalizes signatures and best matches two samples. EERs achieved in experiments with drawing tablets ranged from 0.6 \% to 3.4\% for random forgeries and from 5.4\% to 17.18\% for skilled forgeries. The most common parametric method are Hidden Markov Models \cite{HMMsignatureVan, HMMsignatureTolosana}. These methods determine the probability of sample belonging to the learned model of transitions between observation states. EERs range from 4.6\% to 7.3\% for skilled forgeries, whereas the average for random forgeries EER$\simeq$3\%. A special case of parametric methods with varying number of parameters are the neural networks. In \cite{signature_P_CNN}, approaches employing perceptron networks and convolutional neural networks were compared. The authors analyzed only random forgeries, obtaining EER$\simeq$0\% for convolutional networks.

\begin{table*}[t!]
\renewcommand{\arraystretch}{1.0}
\centering
\footnotesize
\resizebox{\linewidth}{!}{
\begin{tabular}{>{\raggedright\arraybackslash}m{0.11\linewidth}>{\raggedright\arraybackslash}m{0.12\linewidth}>{\raggedright\arraybackslash}m{0.1\linewidth}>{\raggedright\arraybackslash}m{0.12\linewidth}>{\raggedright\arraybackslash}m{0.16\linewidth}>{\raggedright\arraybackslash}m{0.09\linewidth}>{\raggedright\arraybackslash}m{0.10\linewidth}>{\raggedright\arraybackslash}m{0.08\linewidth}} 
\hline

\textit {}  & \cellcolor{blue!30} \textbf{\emph{MobiBits}} &  \textit{MobBIO} &  \textit{MBMA} &  \textit{BioSecure} & \textit{FTV} & \textit{I-Am} & \textit{MOBIO}\\

\hline\hline
{\bf Volunteers} & \cellcolor{blue!30} 53 & 105 & 100 & 713 & 50 & 100 & 150\\\hline
{\bf Acquisition} \linebreak {\bf sessions} & \cellcolor{blue!30} 3 & 1 & 2 & 2 & 1 & 1--3 & 12\\\hline
{\bf Devices used} & \cellcolor{blue!30} Huawei Mate S \linebreak Huawei P9 Lite \linebreak CAT S60& Asus \linebreak Transformer \linebreak Pad TF 300 & iPhone 4s \linebreak Galaxy S2 \linebreak iPad 2 \linebreak Motorola Xoom & Samsung Q1 \linebreak Philips SPC900NC Webcam \linebreak HP iPAQ hx2790 PDA & HP iPAQ rw6100 & Samsung \linebreak Galaxy S4 & Nokia N93i \linebreak MacBook\\\hline
{\bf Biometric} \linebreak {\bf characteristics} \linebreak {\bf included} &  \cellcolor{blue!30} {\bf signatures}  \linebreak {\bf voice}  \linebreak {\bf face}   \linebreak {\bf iris} \linebreak {\bf hand}  & {\bf voice} \linebreak {\bf face} \linebreak {\bf iris} & {\bf voice} \linebreak {\bf signatures} \linebreak {\bf face}  & {\bf voice} \linebreak {\bf signatures} \linebreak {\bf face} \linebreak {\bf fingerprints} & \textbf{face} \linebreak \textbf{teeth} \linebreak \textbf{voice} & \textbf{arm gestures} \linebreak \textbf{ear shape} & \textbf{face} \linebreak \textbf{voice} \\
\hline
\end{tabular}}
\vskip2mm
\caption{Comparison between existing multimodal biometric databases collected with mobile devices and the \textit{MobiBits} database. }
\label {Tab: database_comp}
\end{table*}

\subsection{Face}
Mobile face recognition with locally executed implementations has recently gained interest due to the iPhone X, employing 3D face recognition to authenticate when unlocking the device, using App Store, and Apple Pay \cite{FaceID}. A number of other face recognition applications are available in the app stores for both iOS and Android platforms. Face recognition for images captured in mobile, unconstrained conditions has been a subject of numerous research studies. In 2012, the bi-modal MOBIO database was published along with a description of state-of-the-art evaluation results \cite{MOBIO} for face and speaker biometrics running entirely on a Nokia N93i smartphone. Fusing both characteristics with logistic regression resulted in EER values around 10\%. Only a year later, less than half error rates were achieved using the same database and a range of different methods in the ICB 2013 Face Recognition Evaluation \cite{ICB2013Face}. There are concerns regarding mobile face recognition usability, as expressed in \cite{TimeEvolution}, in which the researchers provided face recognition users with real-time audio feedback rather than written instructions. Also, the importance of interaction time for mobile face recognition was explored. A major concern in face recognition is anti-spoofing, especially considering the popularity of mobile biometrics in payment authentication. A spoof attack database with over 1000 subjects and an efficient face spoof detection system are presented in \cite{FaceSpoofing}.

\subsection{Voice}
Due to its convenient nature and increasing accuracy, text-independent speaker recognition can be a natural choice for mobile authentication mobile systems. The bi-modal MOBIO database \cite{MOBIO}, described in the previous paragraph, contains a broad collection of voice samples captured with a Nokia N93i device. Apart from the evaluation performed in the original work, the database was used as the dataset for the ICB 2013 Speaker Recognition Evaluation \cite{ICB2013Voice}, resulting in encouraging error rates of 5\% EER, further lowered by performing a fusion of all the participating systems, most of them being robust state-of-the-art PLDA systems. Efforts were taken to reduce computational and memory requirements of the i-vector/PLDA systems by simplification and optimization of existing methods \cite{IvectorOptimization, IvectorOptimization2}, resulting in implementations suitable for small-scale devices. Anti-spoofing remains one of the top priorities in speaker recognition and an extensive database, AVSpoof \cite{AVSpoof}, was made available to provide a diverse set of attacks along with an experimental study on speaker recognition vulnerability.

\subsection{Hand}
Hand images can supply information on both hand geometry and texture (wrinkles, principal lines). The authors of \cite{Geometry_mobile} developed a biometric recognition method based on features extracted from hand silhouette and its contour, obtaining EER=3.7\%. According to the works of Zhang \etal~and Sun \etal~\cite{Zhang03onlinepalmprint, Sun_2005_Palmprint}, large areas of the palmar side of the hand provide enough personal information for the identity authentication. In addition, high accuracy hand texture recognition is possible with low-resolution images, less than 100 dpi. Hand could be a useful for biometric authentication thanks to simple acquisition process using the built-in camera on a mobile phone without an additional sensor. Identity recognition methods based on hand images taken with mobile devices mainly rely on texture analysis. The most popular feature extraction methods utilizes a bank of Gabor filters \cite{Kim_palmprint_mobile, Franzgrote_palmprint_mobile}, and SIFT descriptors \cite{Choras_palmprint_mobile}, with best result of EER=0.79\% obtained by Kim \etal \cite{Kim_palmprint_mobile}.



\subsection{Available mobile biometric datasets}
Growing interest in mobile biometrics has recently made multimodal mobile biometric databases very desirable products for both the research community and the industry. Described in the literature and available to the public are the following databases:

The MobBIO database by Sequeira \etal~\cite{MobBio_db} contains data acquired from 105 volunteers with an Asus Transformer Pad TF 300 tablet. It includes voice samples and images of face and iris. The collection of voice samples consists of recordings of 16 sentences for each person, where first 8 were the same for all volunteers and the rest were chosen randomly from a larger set. Mean voice sample duration is 10 seconds. The set of iris images contains 8 images of each eye captured in different lighting conditions, in $300 \times 200$ resolution. As of face images,  there are 16 images of volunteer's face in $640 \times 480$ resolution. Face images were acquired in the same conditions as iris images. 

The BioSecure database by Ortega-Garcia \etal~\cite{BioSecure_db} has been collected at several universities participating in the BioSecure project \cite{BioSecure_db}. It contains data collected in two acquisition sessions from 713 volunteers. Devices used for the acquisition were Samsung Q1 with a Philips SPC900NC Webcam and HP iPAQ hx2790 PDA. Each volunteer provided 4 frontal face images (2 indoors and 2 outdoors), 12 fingerprint samples, 18 voice records (9 indoors and 9 outdoors) and 25 handwritten signatures (15 genuine and 10 skilled forgeries). 

The MBMA database \cite{MMB_Arnowitx_db} contains three characteristics (voice, face, signature) collected in two sessions with four mobile devices, two smartphones (iPhone 4s, Samsung Galaxy S2) and two tablets (iPad 2, Motorola Xoom). Data from the first session was used for system development and it contains samples acquired from 100 volunteers. Each volunteer provided 5 face images, 3 voice records and 6 handwritten signatures. Data from the second session was acquired from 32 people and it contains 4 samples of voice record, 3 face images and 8 signatures. 

The Face-Teeth-Voice collection introduced in \cite{KimFaceTeethVoice2010} (referred to as FTV later on) gathers 1000 biometric traits of three modalities, namely: face images, teeth images, and voice recordings obtained from a portable HP iPAQ rw6100 device in an experiment involving 50 people, hence 20 samples per person, all obtained in a single session. 

Finally, the I-Am database from \cite{AbateArmandEarMobile2017} comprises a combination of physical and behavioral biometrics in the form of ear shape images and accelerometer/gyroscope data, obtained from 100 volunteers in 1 to 3 acquisition sessions, using a Samsung Galaxy S4 mobile phone. The database collects 300 ear shape samples, and 600 accelerometer/gyroscope data samples. 

Table \ref{Tab: database_comp} compares the datasets described above against the {\it MobiBits} database presented in this paper.


\section{Database characteristics}
\label{sec:Database} 
The \emph{MobiBits} multimodal mobile biometric database includes data collected from 53 volunteers (20 female and 33 male), using three different smartphones. The age of subjects ranged from 14 to 71 years. The data were collected in three different sessions organized during three following months. The data were collected twice in each acquisition session, separated by approximately 15 minutes. All samples were acquired in typical office conditions with air conditioning on and ambient temperature set to $24^o$C. Other factors that might have influenced the results, such as gender, age, how frequently the volunteer is using the handwritten signature, health condition, outside environmental conditions before the measurement, time from last meal, {\it etc.}, were recorded and are included in the metadata. Acquisition protocols and descriptions for each biometric characteristic involved in this study are outlined below. 

\subsection{Acquisition devices}
The \emph{MobiBits} database was collected with three smartphones equipped with custom data acquisition software and different technical specifications and capabilities, Table \ref{Tab:devices}. A set of additional accessories was used during the data acquisition process. While collecting handwritten signatures, volunteers used an Adonit Dash 2 stylus to get a smaller, more precise contact point with the display surface. In the thermal imaging sessions, hot pillows, a hairdryer and cooling gel compresses were used to simulate different environmental conditions. In order to constrain the hand position in Session 1, a glass stand with positioning markers was used.

\begin{table}[h!]
\renewcommand{\arraystretch}{1}
\centering
\footnotesize
\resizebox{\linewidth}{!}{
\begin{tabular}{>{\raggedright\arraybackslash}m{0.23\linewidth}>{\raggedright\arraybackslash}m{0.44\linewidth}>{\raggedright\arraybackslash}m{0.25\linewidth}} 
\hline
\textit{Device name}  &  \textit{Features}  &  \textit{Biometric characteristics collected} \\\hline
\hline
{\bf Huawei Mate S} & pressure-sensitive screen \linebreak large 5.5-inch display & signatures \\
& high-quality 13 Mpx  rear camera, large lens aperture of f/2.0 & eyes images \linebreak hand images\\\hline
{\bf Huawei P9 Lite} & mediocre parameters \linebreak average price  & voice recordings\\\hline
{\bf CAT s60} & built-in thermal imaging sensor (resolution of $80\times 60$ pixels, wavelength spectrum of $8-14$ $\mu$m)& face and hand images \linebreak (visible light and thermal images)\\
\hline
\end{tabular}}
\vskip2mm
\caption{Features of mobile devices used for acquisition of certain characteristics.}
\label {Tab:devices}
\end{table}





\subsection{Voice recordings}

The voice samples were captured using a Huawei P9 Lite smartphone in a regular office environment, with AC on and electronic devices working in the background and no artificial noise added. All speakers were speaking Polish language. Samples were recorded during three sessions:

\textbf{Session 1: unconstrained speech} -- the volunteers were asked 8 questions and had to answer them right away in an unconstrained manner.

\textbf{Session 2: reading} -- the volunteers were given excerpts of  "The Little Match Girl" story by Hans Christian Andersen and were asked to read them in a steady and controlled manner.

\textbf{Session 3: testing} -- divided into two sub-sessions:

\textit{a) Enrollment} -- the volunteers were given false identities consisting of a made up name, address, credit card number and the Polish national identification number (PESEL). Following that, they were asked to answer 5 short questions regarding their identities. This session simulates a typical, semi-constrained enrollment situation.

\textit{b) Verification} -- the volunteers were asked 8 questions, similar to the ones in Session 1 but requiring less elaborate answers. This session simulates providing a test sample in a verification scenario.\\

A typical use case of speaker recognition systems is identity verification, so the system evaluation focuses on the comparisons between the Enrollment and Verification sub-sessions. However, different results can be achieved when using the Session 1 or Session 2 as the enrollment data, due to their different nature (unconstrained speech of varying length and long, controlled excerpts, respectively). The dataset can also serve as additional training data for any kind of speaker recognition systems. Fig. \ref{voice_samples} presents an example voice recording.\\

\begin{figure}[t!]
\centering
\includegraphics[width=1\linewidth]{voice_ang.png}
\caption{Visualization of an example voice recording in the time domain: amplitude (top) and frequency spectrum (bottom).}
\label{voice_samples}
\end{figure}


\subsection{On-line handwritten signatures}

\noindent
The on-line handwritten signatures were recorded in real time and the order of the signature components is recorded. Their representations contain $y(x)$ plane coordinates and pressure values for several points on the display surface, all as a function of time. Signatures were collected in three sessions, during which each volunteer was asked to provide 10 signature representations, except for the first session, when 20 signatures were requested. In addition, the database contains $20$ skilled forgeries per person (5 samples provided by 4 different forgers). Having this type of samples is mandatory for testing any method's resistance against spoofing. Example signature samples accompanied by respective skilled forgeries are shown in Fig. \ref{signature_images}.

\begin{figure}[h!]
\centering
\includegraphics[width=1\linewidth]{signatures.png}
\caption{Visualization of an example volunteer's signature on the $y(x)$ plane: genuine signature (left) and skilled forgery (right).}
\label{signature_images}
\end{figure}


\subsection{Face images}
Face images were collected in three different acquisition sessions using a CAT s60 mobile phone. This smartphone allowed collection of two types of images: photos in visible light with a resolution of $480 \times 640$ pixels and thermal images of $240 \times 320$ pixels. Each session includes photos in six poses: frontal images, left and right semi-profile images, left and right profile images and a 'selfie' image.

\begin{figure}[t!]
\centering
\includegraphics[width=1\linewidth]{faces.png}
\caption{Example face images obtained in one session from a single volunteer: photos in visible light (top) and thermal images (bottom).}
\label{face_images}
\end{figure}


\subsection{Iris and periocular}

The data were captured during three acquisition sessions. Rear camera of the Huawei Mate S device has been used to capture images of irises (Session 1) and face focused on the eyes region (Session 2 and 3). The output images were $3120 \times 4160$ pixels in dimensions, which in Session 1 were later manually cropped to only contain either the left or the right eye, while in Sessions 2 and 3 cropped to represent the region with both eyes visible (Fig. \ref{eye_images}). Data collection was carried out in a typical office setting with artificial light sources. Flash was enabled during the first and the third session, and disabled during the second session to simulate different, real-world conditions of authentication.

{\bf Session 1:} 5 images per eye with flash were taken from a 10cm distance

{\bf Session 2:} 5 images of frontal face focused on eye region without flash, 20 cm distance

{\bf Session 3:} 3 images focused on the eye region taken in three face positions: centered face at a 30cm distance (Fig. \ref{eye_images}A), centered face at 20cm distance (Fig. \ref{eye_images}B), face at the angle of 45$^o$ and 30cm distance (Fig. \ref{eye_images}C), without and with flash.


\begin{figure}[ht!]
\centering
\includegraphics[width=1\linewidth]{eyes2}
\caption{Example iris and periocular images obtained in respective acquisition sessions. Numerical indices denote images being taken without (A1, B1, C1) and with flash (A2, B2, C2).}
\label{eye_images}
\end{figure}

\subsection{Hand images}
For hand data, images of palmar and dorsal side of both left and right hand were collected. CAT s60 phone with built-in thermal camera was used for the image acquisition. According to the current state of the art, thermal images have never been used in mobile biometrics. Due to this fact, hand images were collected according to scenario which allows the temperature dynamics analyses to stimulate rich research ideas regarding this fascinating, yet largely unstudied biometric characteristic. Acquisition took place in an office setting with air conditioning set to 24$^o$C. The acquisition sessions are detailed below, while Fig. \ref{hand_images} shows example images acquired in each of them.

{\bf Session 1:} hand images with no temperature influence:\\
\indent\indent
\textit{a)} supported by a glass stand\\
\indent\indent
\textit{b)} in an unconstrained position

{\bf Session 2:} hand images supported by a glass stand:\\
\indent\indent
\textit{a)} after cooling with cold compress\\
\indent\indent
\textit{b)} after warming with a warm pillow

{\bf Session 3:} hand images without support:\\
\indent\indent
\textit{a)} after cooling with cold compress\\
\indent\indent
\textit{b)} after warming with a warm pillow

{\bf *Extra Session:} higher resolution, 13 Mpx camera was used, for more accurate images of the hand texture. Only palmar side was photographed, left and right hand, with and without flash.

\begin{figure}[t!]
\centering
\includegraphics[width=0.8\linewidth]{hands}
\caption{Example hand images in visible light and thermal imaging in three sessions. {\bf Top row:} visible light and thermal representations, with (left) and without the stabilizing glass stand (right). {\bf Middle row:} visible light image and three consecutive thermal images (temperature changes after warming the hand). {\bf Bottom row:} higher resolution camera photos, with and without flash.}
\label{hand_images}
\end{figure}


\begin{table*}[t!]
\renewcommand{\arraystretch}{1.25}
\centering
\footnotesize
\resizebox{\linewidth}{!}{
\begin{tabular}{>{\raggedright\arraybackslash}m{0.07\linewidth}|>{\centering\arraybackslash}m{0.04\linewidth}>{\centering\arraybackslash}m{0.04\linewidth}>{\centering\arraybackslash}m{0.04\linewidth}|>{\centering\arraybackslash}m{0.04\linewidth}>{\centering\arraybackslash}m{0.04\linewidth}>{\centering\arraybackslash}m{0.04\linewidth}|>{\centering\arraybackslash}m{0.04\linewidth}>{\centering\arraybackslash}m{0.04\linewidth}>{\centering\arraybackslash}m{0.05\linewidth}|>{\centering\arraybackslash}m{0.04\linewidth}>{\centering\arraybackslash}m{0.04\linewidth}>{\centering\arraybackslash}m{0.05\linewidth}|>{\centering\arraybackslash}m{0.04\linewidth}>{\centering\arraybackslash}m{0.04\linewidth}>{\centering\arraybackslash}m{0.04\linewidth}|} 


&    \multicolumn{3}{c|}{{\bf VOICE}} &  \multicolumn{3}{c|}{{\bf SIGNATURE}} & \multicolumn{3}{c|}{{\bf FACE}} & \multicolumn{3}{c|}{{\bf IRIS}} &  \multicolumn{3}{c|}{{\bf HAND}}\\
& classes & samples & FTE & classes & samples & FTE & classes & samples & FTE & classes & samples & FTE & classes & samples & FTE\\\hline
\hline
{\bf Session 1 }& 51 & 406 & 0.00\% & 51 & 1020 & 0.00\% & 51 & 778 & 45.89\% &  49 & 497 & 14.69\%  & 49 & 531 & 0.00\%\\
{\bf Session 2 }& 46 & 190 & 0.00\% & 42 & 420 & 0.00\% & 47 & 1 005 & 42.00\% & 46 & 525 & 10.48\%    & 49 & 555 & 0.00\%\\
{\bf Session 3 }& 48 & 624 & 0.00\% & 50 & 500 & 0.00\% & 47 & 1 105 & 40.90\% & 47 & 2 082 & 12.92\%    & - & - &-\\
\hline
\end{tabular}}
\vskip2mm
\caption{A summary of the number of subjects, images and FTE rate for each of the analyzed biometric characteristics.}
\label{fte}
\end{table*}

\section{Database evaluation: tools and protocol}
\label{sec:Evaluation}

\subsection{Biometric toolkit} 
For the evaluation of the \textit{MobiBits} database we performed benchmark calculations of the recognition accuracy for all characteristics: voice samples, signatures, face images, iris images and hand images. Well known biometric methods were used to generate comparison scores and are described briefly in this Section: a proprietary PLDA/i-vector solution for voice recordings, DTW method for signatures, VeriLook for face images, IriCore for the iris images and VGG-based CNN approach for hand images.

{\bf PLDA/i-vector voice recognition} is our proprietary solution based on state-of-the-art PLDA method \cite{PLDAPrince, Ivectors} with 40 mean and variance normalized MFCC features extracted from every signal frame, a 256-component, gender-independent background model \cite{ReynoldsUBM}, 64-dimensional total variability subspace \cite{DehakTVM}, and 64-dimensional PLDA model. The results were normalized using cohort symmetric score normalization \cite{DehakCohorts}. Prior to feature extraction, additional spectral subtraction voice activity detection was performed to remove non-speech periods \cite{Mak2010RobustVA}. The system was trained exclusively on the MOBIO dataset \cite{MOBIO}.

{\bf Dynamic Time Warping} (DTW) is an algorithm used in time series analysis. It is one of the most popular methods for online signature recognition due to its resistance to different signing velocity \cite{DTW_Putz2006}. In this approach  an optimal warping path between two given time series is calculated. Points listed by warping path is next used for comparison.

{\bf VeriLook} is a face recognition technology offered on the market by Neurotechnology \cite{VeriLook}. VeriLook uses a set of robust image processing algorithms, including deep neural networks. It can be used in verification and identification modes, and implements both passive and active types of liveness detection. This technology allows face recognition with non-frontal images, however, the default allowed rotation angles are modest.

{\bf IriCore} is an iris recognition SDK developed by IriTech \cite{IriCore}. The exact algorithm of IriCore iris recognition methodology is not disclosed. As this method is fine-tuned to work with iris images compliant with the ISO/IEC 19794:2011 standard \cite{ISO}, pre-processing of photos was performed: images were manually cropped to $640\times 480$ and converted to grayscale using red channel of the RGB color space, which has been shown to improve the visibility of the iris texture in heavily pigmented irises \cite{TrokielewiczVisibleISBA2016, TrokielewiczBartuziJTIT}.

{\bf VGG-16-Hand} is a method based on the well-known convolutional neural network VGG-16 model \cite{VGGSimonyanCNNsForRecognition2014}, which proved highly accurate in various classification problems. The original model, trained on natural images from the ImageNet dataset, has been fine-tuned using transfer learning on a dataset of hand images from the \textit{Extra Session} with flash enabled, cf. Section \ref{sec:Database}.

\subsection{Logistic regression score fusion} 
In addition to generating comparison scores for each characteristic independently, a score fusions are performed using a logistic regression model, as it has been proven very effective in \cite{LOGIT}, while at the same time can be efficiently implemented on a mobile device, thanks to low computational overhead, when compared with alternative methods, such as calculating likelihood ratios or Gaussian Mixture Modelling.

 Fusing more than one biometric characteristic allows for higher recognition accuracy and enables fine-tuning the combination of methods to the required level of system confidence. Also, attempting a presentation attack on a multimodal biometric system is more difficult, as it necessitates more resources on the attacker's side, compared to attacking a single-modality system, which can still be employed in tasks that don't require very high levels of security. 

Two fusion approaches were tested. The first one took into account only subjects with no missing samples, for whom a fusion of all five characteristics could be performed (Fusion$_1$). Since this was the case only for a limited subgroup of subjects (37 out of 53, or 70\%, Tab. \ref{no_missing_modality}), a second approach is proposed to evaluate the fusion scenario when only selected traits are available. In this fusion scenario, no subjects were discarded and missing comparison scores were substituted with mean values for a given characteristic in the entire score population (Fusion$_2$).

\begin{table}[ht!]
\renewcommand{\arraystretch}{1}
\centering
\footnotesize
\resizebox{\linewidth}{!}{
\begin{tabular}{>{\centering\arraybackslash}m{0.2\linewidth}|>{\centering\arraybackslash}m{0.12\linewidth}|>{\centering\arraybackslash}m{0.12\linewidth}|>{\centering\arraybackslash}m{0.12\linewidth}|>{\centering\arraybackslash}m{0.12\linewidth}|>{\centering\arraybackslash}m{0.12\linewidth}|} 
{\bf number of} & \multicolumn{5}{c|}{\bf number of missing modalities}\\
{\bf subjects }& {\bf 0} & {\bf 1} & {\bf 2} & {\bf 3} & {\bf 4} \\
\hline
\multirow{2}{*}{53} & 37 & 12 & 1 & 1 & 2 \\
 				&69.81\% &	22.64\% & 1.89\% & 1.89\% & 3.77\%\\
 				\hline
\end{tabular}}
\vskip2mm
\caption{Summary of modality availability in respect to subjects in the database.}
\label{no_missing_modality}
\end{table}

\subsection{Enrollment and verification performance:\\evaluation protocol and metrics}
IriCore and VeriLook engines performed quality checks prior to feature extraction and biometric template creation. Since some samples were not accepted, a standard performance metrics, {\it failure-to-enroll rates (FTEs)}, were calculated to express the ratio of rejected samples to all samples.
 
Each biometric characteristic and each fusion model was tested in a verification scenario. Averaged Receiver Operating Characteristics (ROCs) and mean Equal Error Rates (EERs) were used to present the verification accuracy. These performance metrics were calculated for comparison scores obtained by performing all possible comparisons within the data, excluding the within-session ones, as these would involve comparing highly correlated data. 
		
\section{Results and discussion}
\label{sec:Results}
FTEs calculated for all tested characteristics are presented in Table \ref{fte}. High FTE values for the face engine may be explained with some images being low quality (CAT s60 thermal module photos) and difficulties in detecting and processing profile images by the VeriLook software, as the profile and semi-profile images constituted around 60\% of the total image count. Iris enrollment errors can be a result of using mixed quality grayscale images converted from RGB images, instead of high quality, infrared light images recommended by the IriCore software manual. Other problems may be related to: iris image noise, reflections on the eye, lashes, squinted eyes. The voice recognition engine, the CNN approach using hand images and the DTW method did not perform any quality checks, hence the zero FTE rates.

The ROC curves together with respective EER values are shown in Figure \ref{roc_curves}. The best single characteristic results can be achieved with face recognition, which allows verification accuracy with EER=$2.32\%$. Score fusions further improve these results, allowing $0.36\%$ EER when fusing scores from all five characteristics. However, even in cases when selected modalities are unavailable (true for 30\% of subjects), the system is able to provide satisfactory results of 4.23\% EER using Fusion$_2$ approach, which, although yielding worse results than the face modality alone, potentially offers much higher security against counterfeit attempts, such as providing a print-out sample.  


\begin{figure}[t!]
\centering
\includegraphics[width=1\linewidth]{ROCs}%\hskip1mm
%\includegraphics[width=0.49\linewidth]{DETs}
\caption{ROC curves for each modality and for two fusion scenarios using all of the five considered characteristics.}
\label{roc_curves}
\end{figure}



\section{Conclusions}
\label{sec:Conclusions}
This paper presents a few important deliverables, most notably the multimodal mobile biometric database -- \emph{MobiBits}, comprising samples of five different biometric characteristics, collected using three different mobile devices, including a novel type of samples: thermal images of hands and faces, acquired using a thermal sensor equipped mobile phone. A comprehensive set of experiments conducted on the data is reported to show the example benchmark accuracy that can be achieved on the dataset using selected commercial and academic recognition solutions, including a methodology for fusing scores for individual characteristics as a way of improving recognition reliability. The \emph{MobiBits} database is offered publicly at no cost for non-commercial research purposes. We hope that this will constitute a worthy addition to the mobile biometric datasets available for the biometrics community, and will provide an incentive for further research in the field of mobile biometrics. For further information on how to get access to the data please contact the authors at: \url{link_removed_for_peer_review}.   
   
% \section{Acknowledgement}

{\small
\bibliographystyle{ieee}
\bibliography{refs}
}

\end{document}

