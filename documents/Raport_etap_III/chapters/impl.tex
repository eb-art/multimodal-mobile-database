W niniejszym rozdziale opisano implementacje silnika rozpoznawania w dwóch środowiskach oraz zastosowanie jednej z implementacji w aplikacji demonstracyjnej. Rozdział rozpoczęto wylistowaniem i krótkim opisem funkcji  \textit{MATLAB} realizujących bezpośrednio algorytmy rozpoznawania tożsamości na podstawie nagrań mowy. Następnie, po wprowadzeniu do platform \textit{.NET}, \textit{Xamarin} i wykorzystywanej biblioteki do obliczeń numerycznych, przedstawiono implementację biblioteki rozpoznawania mówców \textit{RVLib}, a także jej praktyczne zastosowanie w aplikacji demonstrującej biometryczną weryfikację tożsamości.

\section{Zasada działania systemu}
W ogólnym ujęciu, system z wykorzystaniem metod modelowania probabilistycznego mapuje potencjalnie nieograniczonej długości nagrania mowy swobodnej (ang. \textit{text-independent speech}) na przestrzeń o skończonej wymiarowości. Wektor określający współrzędne nagrania w takiej przestrzeni nazywany jest tzw. $i$-wektorem. Pary $i$-wektorów mogą być badane z użyciem modeli PLDA (ang. \textit{probabilistic linear discriminant analysis}) pod kątem pochodzenia z nagrań zawierających mowę tej samej osoby. W celu dogłębnego zapoznania się z teoretycznymi podstawami tych metod, dobrymi punktami startowymi będą publikacje: \cite{eigenvoice, bayes, normalization}.

\section{Implementacja w środowisku \textit{MATLAB}}
Bazową implementacją systemu, w kontekście usuwania defektów i przeprowadzania testów, jest implementacja w środowisku \textit{MATLAB}. W przeciwieństwie do implementacji w typowych językach użytkowych, daje ona dużą kontrolę nad danymi będącymi wynikami działania poszczególnych komponentów systemu, co ułatwia śledzenie błędów. Każda zmiana funkcji biblioteki \textit{RVLib} powinna zatem być uprzednio testowana w tej wersji.
\subsection{Opis komponentów}
Dalej scharakteryzowano funkcje realizujące poszczególne komponenty systemu. W plikach źródłowych znajdują się detale dotyczące dokładnej postaci przyjmowanych argumentów i zwracanych obiektów.
\subsubsection{Detekcja mowy}
Detekcja mowy służy do znalezienia w nagraniach fragmentów, w których znajduje się ludzki głos. Ten etap działania jest szczególnie przydatny do testów ze względu na różną formę danych testowych - w działaniu operacyjnym zwykle trudno określić optymalne parametry detektora mowy, więc wersja mobilna pozbawiona jest tego komponentu. Funkcje detekcji mowy zgromadzono w katalogu \textit{VAD}.
\begin{itemize}
\item{\textit{detect\_voice.m} - główna funkcja. Zwracanie fragmentów mowy na podstawie nagrania i zadanych parametrów.}
\item{\textit{get\_spectrum.m} - obliczanie widma nagrania.}
\item{\textit{reconstruct\_signal.m} - rekonstrukcja sygnału na podstawie widma.}
\item{\textit{remove\_dc\_offset.m} - usuwanie składowej stałej z sygnału.}
\item{\textit{zcr.m} - obliczanie współczynnika przejść przez zero.}
\end{itemize}
\subsubsection{Ekstrakcja cech MFCC}
Ekstrakcja cech MFCC jest podstawowym etapem w działaniu systemów rozpoznawania mówców. Polega ona na podzieleniu sygnału mowy na nachodzące na siebie ramki o niewielkiej długości (np. $25$ms) i określeniu pewnych własności widma sygnału w tych ramkach. Funkcje ekstrakcji cech MFCC zgromadzono w katalogu \textit{MFCC}.
\begin{itemize}
\item{\textit{extract\_feature\_vectors.m} - główna funkcja. Obliczanie zestawów cech MFCC dla nagrań w zadanym katalogu.}
\item{\textit{mfcc.m} - obliczanie zestawu cech MFCC dla konkretnego nagrania.}
\item{\textit{mel\_filters.m} - obliczanie położenia zadanej liczby filtrów Mel dla określonej częstotliwości próbkowania.}
\item{\textit{get\_mel\_filter\_value.m} - obliczanie wartości filtrów Mel dla zadanej częstotliwości.}
\item{\textit{from\_mel.m} - przekształcenie ze skali Mel do skali liniowej.}
\item{\textit{wcmvn.m} - okienkowa normalizacja średniej i wariancji cech MFCC.}
\end{itemize}
\subsubsection{Uczenie modeli GMM}
Modelowanie mówców w systemach rozpoznawania tożsamości zwykle opiera się o tzw. modelowanie tła, które wymaga nauczenia odpowiednich modeli mieszanin gaussowskich (tzn. pewnej ważonej sumy zmiennych z wielowymiarowych rozkładów normalnych). Funkcja uczenia modeli GMM znajduje się w pliku \textit{gmm.m} w katalogu \textit{GMM}. W trakcie uczenia co iterację wypisywana jest suma funkcji wiarygodności dla wszystkich zadanych wektorów MFCC w celu monitorowania zbieżności algorytmu.
\subsubsection{Uczenie modeli TVM i ekstrakcja $i$-wektorów}
Aby zmapować potencjalnie nieograniczonej długości nagranie mowy na stałego rozmiaru wektory liczb zmiennoprzecinkowych, wykorzystuje się modele całkowitej zmienności (ang. \textit{total variability modelling}) i $i$-wektory. Funkcje uczenia modelu całkowitej zmienności zgromadzono w katalogu \textit{TVM}. W trakcie uczenia co iterację wypisywana jest suma elementów macierzy precyzji dla oszacowań $i$-wektorów ze wszystkich nagrań w celu monitorowania zbieżności. Użytkownik może obserwować osiągany w każdej iteracji współczynnik błędu zrównoważonego (EER) dla dodatkowo określonych danych testowych i klasyfikacji wykonywanej za pomocą odległości kosinusowej.
\begin{itemize}
\item{\textit{tvm.m} - uczenie modeli całkowitej zmienności.}
\item{\textit{find\_eer\_cosine.m} - wyznaczanie EER dla odległości kosinusowej i zadanych $i$-wektorów testowych oraz ich etykiet.}
\item{\textit{extract\_ivector.m} - wyznaczanie $i$-wektora dla określonego modelu tła i zbioru cech MFCC.}
\item{\textit{normalize\_ivector.m} - normalizacja $i$-wektora.}
\item{\textit{train\_normalization.m} - wyznaczanie parametrów normalizacji, w tym macierzy przekształcenia wybielającego.}
\end{itemize}
\subsubsection{Uczenie modeli PLDA}
Wyznaczone $i$-wektory porównuje się z użyciem modeli probabilistycznej liniowej analizy dyskryminacyjnej - PLDA. Funkcja uczenia modeli PLDA znajduje się w pliku \textit{plda.m} w katalogu \textit{PLDA}. Z uwagi na wysoką szybkość działania, nie prezentuje się pośrednich wyników rozpoznawania po każdej iteracji.
\subsubsection{Ocena podobieństwa i normalizacja wyników}
Funkcje oceny podobieństwa i normalizacji wyników znajdują się w katalogu \textit{SCORING}.
\begin{itemize}
\item{\textit{score\_plda.m} - porównywanie $i$-wektorów z wykorzystaniem modelu PLDA.}
\item{\textit{normalize\_results.m} - normalizacja i agregacja wyników porównań PLDA.}
\item{\textit{aggregate\_scores.m} - agregacja wyników porównań funkcją średniej.}
\item{\textit{find\_eer.m} - wyznaczanie EER dla zadanych wyników porównań i macierzy zgodności tożsamości $i$-wektorów w zbiorach referencyjnym i testowym.}
\item{\textit{find\_matches.m} - wyznaczanie macierzy zgodności tożsamości na podstawie dwóch wektorów etykiet.}
\end{itemize}
\subsubsection{Pozostałe}
Pozostałe funkcje znajdują się w katalogu \textit{OTHER}.
\begin{itemize}
\item{\textit{save\_model.m} - zapis wszystkich elementów modelu do plików CSV (ang. \textit{comma-separated values}) według struktury katalogów wykorzystywanej później w bibliotece \textit{RVLib}.}
\end{itemize}

\section{Wykorzystane narzędzia}
Dzięki wieloplatformowym implementacjom platformy programistycznej \textit{.NET} możliwe było zaimplementowanie jednej biblioteki możliwej do uruchomienia w niemal dowolnym środowisku sprzętowym o wystarczających zasobach obliczeniowych i pamięciowych. Dalej opisano tę platformę oraz zestaw narzędzi \textit{Xamarin} pozwalający na wytwarzanie w języku \textit{C\#} i w oparciu o \textit{.NET Framework} aplikacji korzystających z natywnych rozwiązań platform iOS czy Android.

\subsection{Platforma programistyczna \textit{.NET}}
Platforma programistyczna \textit{.NET Framework} jest rozwiązaniem rozwijanym przez Microsoft Corporation od 2002 roku. Technologia ta nie jest związana z konkretnym językiem programowania i umożliwia tworzenie kodu w jednym z wielu języków (\textit{C++}, \textit{C\#}, \textit{Visual Basic} i inne). Pełna obsługa platformy występuje pod systemami tworzonymi przez Microsoft Corporation, jednak istnieją też implementacje pod inne środowiska programowo-sprzętowe, na przykład wieloplatformowe \textit{Mono}.

Platforma definiuje środowisko uruchomieniowe \textit{Common Language Runtime} (CLR) i pojęcie tzw. kodu zarządzanego (ang. \textit{managed code}). W przypadku typowego programu napisanego w języku niższego poziomu, na przykład \textit{C/C++}, program jest zapisanym binarnie zestawem instrukcji, który system operacyjny ładuje do pamięci i uruchamia. Ciężar zarządzania pamięcią czy zagadnień bezpieczeństwa spoczywa na programiście, co może być znacznym utrudnieniem w przypadku tworzenia wysokopoziomowego, abstrakcyjnego kodu. Kod zarządzany cechuje zarządzanie jego wykonaniem przez platformę CLR. W momencie kompilacji, programy napisane w jednym z obsługiwanych języków wysokiego poziomu (na przykład \textit{C\#}) są przekształcane do tzw. kodu pośredniego (ang. \textit{intermediate code}) platformy, który sama platforma kompiluje do kodu maszynowego dopiero przed pierwszym uruchomieniem. Aby mówić o kodzie zarządzanym, należy zatem wziąć pod uwagę konieczność abstrakcji od konkretnej platformy i rozwiązań natywnych. Bogata biblioteka standardowa platformy \textit{.NET Framework} i liczne zarządzane biblioteki zewnętrzne ułatwiają to zadanie.

\subsection{Platforma mobilna \textit{Xamarin}}
Tworzenie aplikacji mobilnych w języku \textit{C\#} na dowolną popularną platformę mobilną (Android, iOS, Windows Phone) jest możliwe dzięki platformie mobilnej \textit{Xamarin}. W oparciu o \textit{Mono}, umożliwia ona korzystanie z funkcji platformy \textit{.NET} w połączeniu z zestawem możliwości natywnych, specyficznych dla SDK mobilnych systemów operacyjnych i udostępnianych przez elementy platformy takie, jak \textit{Xamarin.Android} czy \textit{Xamarin.iOS}. Interfejsy graficzne platformy \textit{Xamarin} mogą być zarówno stworzone z użyciem elementów natywnych, jak i wspólnego interfejsu \textit{Xamarin.Forms}.

W niniejszej pracy wytworzono bibliotekę \textit{RVLib} i wykorzystano ją w aplikacji systemu Android \textit{RVApp}. Dzięki platformie \textit{Xamarin} możliwe było w szczególności korzystanie z niskopoziomowych, natywnych możliwości zbierania nieskompresowanych próbek dźwiękowych, a następnie przetwarzanie ich z użyciem znacznie bardziej abstrakcyjnego kodu bibliotecznego. 

Aby móc komfortowo zaimplementować mechanizmy rozpoznawania mówców w typowym języku wysokiego poziomu, a nie w środowisku ukierunkowanym na obliczenia matematyczne, należało skorzystać z istniejącej biblioteki do obliczeń numerycznych. Wymagania funkcjonalne takie, jak możliwości obliczeń macierzowych czy podstawowe funkcje statystyczne spełnia biblioteka typu open-source \textit{Math .NET Numerics}. Pełny zestaw funkcji biblioteki jest napisany w czystym kodzie zarządzanym, zatem nie wprowadza ona dodatkowych utrudnień w implementacji rozwiązań wieloplatformowych.

\section{Biblioteka rozpoznawania mówców \textit{RVLib}}
Biblioteka \textit{RVLib} zawiera implementację algorytmów opisanych w poprzednich częściach pracy z pominięciem algorytmów uczących i algorytmu detekcji mowy. Zakłada się, że odpowiednie modele uczone są w środowisku \textit{MATLAB} i eksportowane funkcją \textit{save\_model} do plików CSV w odpowiedniej strukturze katalogów. Parametry pracy biblioteki są następujące:
\begin{itemize}
\item{Obsługa częstotliwości próbkowania $16$kHz.}
\item{Uśrednianie kanałów stereo, jeżeli plik nie zawiera dźwięku monofonicznego.}
\item{Obsługa formatów WAV, MP3 oraz samodzielnych, 16-bitowych próbek PCM.}
\end{itemize}

\subsection{Klasy biblioteki}
Biblioteka definiuje przestrzeń nazw \textit{RVLib} zawierającą trzy widoczne na zewnątrz klasy:
\begin{itemize}
\item{\textit{Engine} - ,,silnik'' biometryczny, umożliwiający ekstrakcję $i$-wektorów z wczytanych nagrań audio oraz późniejsze ich porównywanie.}
\item{\textit{Recording} - nagranie audio załadowane z dysku.}
\item{\textit{IVector} - $i$-wektor, odpowiednik wzorca biometrycznego.}
\end{itemize}
Dalej przedstawiono publiczny interfejs wystawiany twórcom aplikacji przez bibliotekę.

\subsubsection{Klasa \textit{Engine}}
Publiczny interfejs klasy \textit{Engine} składa się z konstruktora oraz czterech metod. Metoda porównująca $i$-wektory przyjmuje zestaw $i$-wektorów referencyjnych, dla których agregowane są punkty porównań z $i$-wektorem testowym, a także wykonywana jest normalizacja wyników na podstawie kohort wyeksportowanych ze środowiska \textit{MATLAB}.
\begin{lstlisting}[caption={Konstruktor inicjalizujący obiekt \textit{Engine} na podstawie ścieżki do modelu wyeksportowanego ze środowiska \textit{MATLAB}.},captionpos=b,label={engine_ctor1}]
public Engine(string dataPath)
\end{lstlisting}

\begin{lstlisting}[caption={Metoda zwracająca $i$-wektor wygenerowany na podstawie zadanego nagrania.},captionpos=b,label={engine_enroll}]
public IVector EnrollFromRecording(Recording recording)
\end{lstlisting}

\begin{lstlisting}[caption={Metoda porównująca dany zestaw $i$-wektorów referencyjnych jednej osoby z $i$-wektorem testowym.},captionpos=b,label={engine_compare}]
public double CompareIVectors(List<IVector> referenceIVectorsList, IVector testIVector)
\end{lstlisting}

\begin{lstlisting}[caption={Metoda zwracająca oszacowanie wartości FNMR przy danym progu punktowym.},captionpos=b,label={true_proba}]
public double GetTrueProba(double result)
\end{lstlisting}

\begin{lstlisting}[caption={Metoda zwracająca oszacowanie wartości FMR przy danym progu punktowym.},captionpos=b,label={true_proba}]
public double GetNegativeProba(double result)
\end{lstlisting}

\subsubsection{Klasa \textit{Recording}}
Publiczny interfejs klasy \textit{Recording} składa się z dwóch konstruktorów:
\begin{lstlisting}[caption={Konstruktor inicjalizujący obiekt \textit{Recording} na podstawie ścieżki do pliku.},captionpos=b,label={recording_ctor2}]
public Recording(string filePath)
\end{lstlisting}
\begin{lstlisting}[caption={Konstruktor inicjalizujący obiekt \textit{Recording} na podstawie tablicy próbek i informacji o częstotliwości próbkowania.},captionpos=b,label=recording_ctor2]
public Recording(float[] samples, int samplingRate)
\end{lstlisting}

\subsubsection{Klasa \textit{IVector}}
Klasa \textit{IVector} służy wyłącznie za magazyn dla wektorów liczb zmiennoprzecinkowych, zatem nie definiuje się żadnego publicznego interfejsu widocznego poza biblioteką. Istnienie klasy jest uzasadnione możliwością późniejszego nieskomplikowanego rozszerzania klasy o dodatkowe pola i metody, na przykład ułatwiające przechowywanie informacji o jakości nagrania bazowego lub innych metadanych.

\section{Aplikacja demonstracyjna \textit{RVApp}}
Aplikacja demonstracyjna \textit{RVApp} pozwala na przeprowadzenie weryfikacji tożsamości w oparciu o do pięciu nagrań referencyjnych i jedno testowe. Modele, zgodnie z opisem biblioteki, ładowane są ze wskazanego katalogu w pamięci smartfona. Aplikacja została wykonana z wykorzystaniem platformy \textit{Xamarin} i \textit{Mono}. Próbki PCM nagrań głosu są pobierane z użyciem niskopoziomowego API platformy Android, \textit{AudioRecord}. Wysokopoziomowe API \textit{MediaRecorder} umożliwia wyłącznie nagrywanie dźwięku skompresowanego przez jeden z wbudowanych koderów. Do aplikacji załączono modele, których wyniki testów przedstawiono w kolejnej sekcji.

\begin{figure}[h]
\begin{subfigure}{0.5\textwidth}
\includegraphics[width=1.0\textwidth]{./graphics/scr1.png}
\end{subfigure}
\begin{subfigure}{0.5\textwidth}
\includegraphics[width=1.0\textwidth]{./graphics/scr2.png}
\end{subfigure}
\caption{Zrzuty ekranu z aplikacji \textit{RVApp}}
\label{screenshots}
\end{figure}

Sposób wykorzystania aplikacji jest następujący:
\begin{enumerate}
\item{Nagranie do pięciu nagrań testowych i nagrania testowego w opisany sposób:}
\begin{enumerate}
\item{Wybranie odpowiedniej etykiety.}
\item{Nacisnięcie przycisku ,,Nagrywaj'' i rozpoczęcie mowy.}
\item{Naciśnięcie przycisku ,,Zatrzymaj'' po zakończeniu mowy.}
\item{Ewentualne odsłuchanie nagranego głosu po naciśnięciu ,,Odtwórz''.}
\end{enumerate}
\item{Wybór, które nagrania mają być włączone do zbioru nagrań porównywanych z testowym poprzez zaznaczenie opcji ,,Tak'' lub ,,Nie'' obok etykiet.}
\item{Naciśnięcie przycisku ,,Twórz wzorzec'' i oczekiwanie na ekstrakcję $i$-wektorów.}
\item{Naciśnięcie przycisku ,,Porównaj'' i oczekiwanie na wynik porównania.}
\end{enumerate}
Prezentowane użytkownikowi obok uzyskanych punktów porównania liczby ,,P-Prawdziwy'' i ,,P-Fałszywy'' to prawdopodobieństwa popełnienia błędów odpowiednio fałszywego odrzucenia i fałszywej akceptacji przy odrzuceniu lub przyjęciu hipotezy o pochodzeniu $i$-wektorów referencyjnych i testowego od tej samej osoby. W prezentowanym przypadku bardziej prawdopodobne jest, że $i$-wektor testowy należy do tej samej osoby co $i$-wektory referencyjne (,,P-Prawdziwy'' $<$ ,,P-Fałszywy'').

\section{Wyniki testów silnika rozpoznawania}
W procesie tworzenia systemu nauczono trzy różne modele o odmiennych hiperparametrach - nazywane dalej \textit{ICB}, \textit{BITS} i \textit{MOBI} - uszeregowane w kolejności od najbardziej do najmniej skomplikowanego. Najmniej skomplikowany model \textit{MOBI} załączono jako część biblioteki i aplikacji demonstracyjnej, gdyż łączy on wysoką jakość rozpoznawania i szybkość działania. Opracowany silnik porównano z narzędziem \textit{Neurotechnology VeriSpeak 10.0} pod kątem jakości weryfikacji oraz identyfikacji tożsamości.

\subsection{Porównanie z \textit{Neurotechnology VeriSpeak 10.0}}
\textit{Neurotechnology VeriSpeak 10.0} jest narzędziem o ugruntowanej pozycji rynkowej, ale cechuje je wysoka cena ($\sim300${\euro}/stanowisko w najuboższej wersji) i skomplikowany model licencyjny. Porównano nauczone modele z tym komercyjnym narzędziem z wykorzystaniem nagrań z sesji 3/4 bazy \textit{MobiBits}.

\begin{figure}[h]
\includegraphics[width=1.0\textwidth]{./graphics/voice/NEURO_roc_MF.eps}
\label{roc_neuro_mf}
\caption{Krzywa ROC dla wyznaczonych modeli i \textit{VeriSpeak 10.0}.}
\end{figure}

\begin{figure}[h]
\includegraphics[width=1.0\textwidth]{./graphics/voice/NEURO_cmc_MF.eps}
\label{cmc_neuro_mf}
\caption{Krzywa CMC dla wyznaczonych modeli i \textit{VeriSpeak 10.0}.}
\end{figure}

\begin{table}[h]
\centering
\resizebox{\textwidth}{!}{\begin{tabular}{|c|c|c|c|c|c|}
\hline
\textbf{Narzędzie} & \textbf{EER} & \textbf{CMC - pozycja 1} & \textbf{CMC - pozycja 3} & \textbf{CMC - pozycja 5} & \textbf{CMC - pozycja 10}\\
\hline
\textit{RVLib - model ICB} & 11.14 & 0.471 & 0.742 & 0.836 & 0.948\\
\hline
\textit{RVLib - model BITS} & 10.63 & 0.539 & 0.776 & 0.846 & 0.960\\
\textit{RVLib - model MOBI} & 10.90 & 0.508 & 0.778 & 0.878 & 0.953\\
\textit{VeriSpeak} & 10.68 & 0.788 & 0.886 & 0.928 & 0.960\\
\hline
\end{tabular}}
\caption{EER i wartości krzywej CMC w czterech punktach dla bazy \textit{MobiBits} i narzędzia \textit{VeriSpeak 10.0}.}
\label{neuro_comparison}
\end{table}

Opracowane oprogramowanie działa niewiele gorzej od narzędzia komercyjnego w trybie identyfikacji - jedynie pierwsze pozycja krzywej CMC mocniej odbiegają od wyniku \textit{VeriSpeak 10.0}. W trybie weryfikacji, osiągane wyniki w najbardziej interesującym otoczeniu punktu EER są już bardzo zbliżone. Samo EER dla modelu \textit{BITS} jest niższe o $0.05$. W zastosowaniu mobilnym, biometria głosu może zostać wykorzystana przede wszystkim do odblokowywania dostępu do wrażliwych zasobów zdalnych lub zgromadzonych na urządzeniu, więc najistotniejszy jest tryb weryfikacji. Biorąc pod uwagę koszt narzędzia \textit{VeriSpeak 10.0}, skomplikowany model licencyjny i jakość rozpoznawania w okolicy EER, zaimplementowany system można uznać za konkurencyjny. Ponadto, system może być z powodzeniem rozwijany, a modele uczone na nowo z wykorzystaniem bogatszych lub specyficznych zbiorów danych, co czyni \textit{RVLib} rozwiązaniem znacznie elastyczniejszym.